import React, { useEffect, useMemo, useState } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import Ranking from '../components/Ranking'
import Card from '../ui/Card'
import Title from '../ui/Title'
import { convertApiRoundToDisplayRound } from '../utils/rounds'
import { getRanking } from '@breci/eventz_api'

const EventRanking = ({ rounds, users, order = 'SCORE_ASCENDING' }) => {
  const [roundsRanks, setRoundsRanks] = useState([])
  useEffect(() => {
    Promise.all(
      rounds.map((round) =>
        getRanking(round.id).then((resp) => resp.data.content)
      )
    ).then((data) => {
      setRoundsRanks(data)
    })
  }, [rounds])
  const roundData = useMemo(() => {
    if (users && roundsRanks) {
      return convertApiRoundToDisplayRound(roundsRanks, users)
    }
    return null
  }, [roundsRanks, users])

  const roundsWithUsers = useMemo(() => {
    return Object.entries(roundData).map(([userId, userData]) => {
      const userInfo = users.find((u) => u.accountId === userId)
      return {
        ...userData,
        user: userInfo
      }
    })
  }, [users, roundData])

  return (
    <Card style={{ width: '100%' }}>
      <StyledTitle theme="dark">CLASSEMENT</StyledTitle>
      <StyledRanking
        data={roundsWithUsers}
        nbRounds={rounds.length}
        order={order}
      />
    </Card>
  )
}

const StyledRanking = styled(Ranking)`
  ${tw`w-full`}
`

const StyledTitle = styled(Title)`
  ${tw`mb-6`}
`

export default EventRanking
