import { useCallback, useMemo, useState } from 'react'
import { createContainer } from 'unstated-next'
import messages_fr from '../translations/fr'
import messages_en from '../translations/en'
import { useIntl } from 'react-intl'

function flattenMessages(nestedMessages, prefix = '') {
  return Object.keys(nestedMessages).reduce((messages, key) => {
    let value = nestedMessages[key]
    let prefixedKey = prefix ? `${prefix}.${key}` : key
    if (typeof value === 'string') {
      messages[prefixedKey] = value
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey))
    }
    return messages
  }, {})
}

const messagesDictionary = {
  fr: flattenMessages(messages_fr)
  // en: flattenMessages(messages_en)
}

const language = navigator.language.split(/[-_]/)[0] // language without region code

const checkedLanguage = Object.keys(messagesDictionary).includes(language)
  ? language
  : 'fr' // default

function useLanguageHook(initialState = checkedLanguage) {
  let [language, setLanguage] = useState(initialState)

  const availableLanguages = useMemo(() => Object.keys(messagesDictionary), [])

  const messages = useMemo(() => {
    return messagesDictionary[language]
  }, [language])

  const changeLanguage = useCallback(
    (locale) => {
      if (availableLanguages.includes(locale)) {
        setLanguage(locale)
      }
    },
    [setLanguage, availableLanguages]
  )

  return { language, changeLanguage, messages, availableLanguages }
}

let Language = createContainer(useLanguageHook)

export const LanguageProvider = Language.Provider
export const useLanguage = Language.useContainer
