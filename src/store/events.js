import { useCallback, useMemo, useState } from 'react'
import { createContainer } from 'unstated-next'
import {
  createTeam,
  getEvent,
  getEvents,
  getRounds,
  getTeams,
  deleteTeam
} from '@breci/eventz_api'
import { toDate, isAfter, isWithinInterval, isSameDay } from 'date-fns'
import config from '../config'

/*
interface SelectedEventInterface{
  event: Event
  rounds: Round[]
}
*/

// TODO move all that in hooks
function expandEventProperties(event) {
  let now = new Date()
  event.closeRegistrationsAtDate = toDate(event.closeRegistrationsAt * 1000)
  event.endEventAtDate = toDate(event.endEventAt * 1000)
  event.openRegistrationsAtDate = toDate(event.openRegistrationsAt * 1000)
  event.startEventAtDate = toDate(event.startEventAt * 1000)
  event.isFuture = isAfter(event.startEventAtDate, now)
  event.isPast = isAfter(now, event.endEventAtDate)
  event.isLive = isWithinInterval(now, {
    start: event.startEventAtDate,
    end: event.endEventAtDate
  })
  event.isRegistrationOpen = isWithinInterval(now, {
    start: event.openRegistrationsAtDate,
    end: event.closeRegistrationsAtDate
  })
  event.lastOneDay = isSameDay(event.startEventAtDate, event.endEventAtDate)

  // Temporary placeholder for images
  event.images = {
    header: '',
    background: '',
    logo: ''
  }
}

function useEventsHook(
  initialState = {
    events: [],
    selectedEvent: { event: null, rounds: [], teams: [] }
  }
) {
  const [events, setEvents] = useState(initialState.events)
  const [selectedEvent, setSelectedEvent] = useState(initialState.selectedEvent)

  // get the next upcoming event

  // TODO fix error where past event can be displayed as the next event
  const nextEvent = useMemo(() => {
    let next = null
    for (let i = 0; i < events.length; i++) {
      const event = events[i]
      // if event is started
      if (
        event.startEventAt * 1000 < Date.now() &&
        event.endEventAt * 1000 > Date.now()
      ) {
        if (!next) {
          next = event
        } else {
          if (next.startEventAt > event.startEventAt) {
            next = event
          }
        }
      } else {
        if (next && event.startEventAt * 1000 > Date.now()) {
          if (
            next.startEventAt * 1000 < Date.now() &&
            next.endEventAt * 1000 > Date.now()
          ) {
            // do nothing
          } else if (next.startEventAt > event.startEventAt) {
            next = event
          }
        } else if (!next) {
          next = event
        }
      }
    }
    return next
  }, [events])

  const [selectedEventTeams, setSelectedEventTeamsState] = useState([])
  const [loadingTeams, setLoadingTeams] = useState(false)
  const [loading, setLoading] = useState(false)

  const addEvents = useCallback((newEvents) => {
    setEvents((_events) => [
      ..._events.filter((event) => !newEvents.find((e) => event.id === e.id)),
      ...newEvents
    ])
  }, [])

  const loadEvents = useCallback(async () => {
    setLoading(true)
    const response = await getEvents(config.API_ORGANISATION_ID)

    response.data.content.forEach((element) => {
      expandEventProperties(element)
    })

    addEvents(response.data.content)
    setLoading(false)
  }, [addEvents])

  const loadEvent = useCallback(
    async (eventId) => {
      const e = events.find((_e) => _e.id === eventId)
      if (e) {
        // TODO remove once EventParticipants uses meta instead
        expandEventProperties(e)
        const v = {
          event: e,
          rounds: (await getRounds(eventId, { size: 1000, page: 0 })).data
            .content,
          teams: (await getTeams({ size: 1000, page: 0 }, eventId)).data.content
        }
        setSelectedEvent(v)

        return e
      }
      setLoading(true)
      const [eventResp, roundsResp, teamsResp] = await Promise.all([
        getEvent(config.API_ORGANISATION_ID, eventId),
        getRounds(eventId, { size: 1000, page: 0 }),
        getTeams({ size: 1000, page: 0 }, eventId)
      ])

      // TODO remove once EventParticipants uses meta instead
      expandEventProperties(eventResp.data)
      const v = {
        event: eventResp.data,
        rounds: roundsResp.data.content,
        teams: teamsResp.data.content
      }

      setSelectedEvent(v)

      addEvents([eventResp.data])
      setLoading(false)
      return eventResp.data
    },
    [events, addEvents]
  )

  const loadSelectedEventTeams = useCallback(async () => {
    if (selectedEvent) {
      setLoadingTeams(true)
      setSelectedEventTeamsState([])
      setLoadingTeams(false)
    }
  }, [selectedEvent])

  /**
   * @param eventId string
   */
  const joinEvent = useCallback(async (eventId, userName) => {
    return createTeam(eventId, { tag: 'FFS', name: userName })
  }, [])

  /**
   * @param eventId
   * @type {function(*=, *=): Promise<{data: *, statusCode: *}> | Promise<never> | undefined}
   */
  const leaveEvent = useCallback(async (eventId) => {
    return deleteTeam(eventId)
  }, [])

  const clearEvents = useCallback(() => {
    setEvents([])
  }, [])

  const addTeamToSelectedEvent = useCallback((team) => {
    setSelectedEvent((e) => {
      if (e) {
        return {
          ...e,
          teams: [...e.teams, team]
        }
      } else return e
    })
  })

  const deleteTeamToSelectedEvent = useCallback((team) => {
    setSelectedEvent((e) => {
      if (e) {
        return {
          ...e,
          teams: e.teams.filter((t) => t.id !== team.id)
        }
      } else return e
    })
  })

  const refreshSelectedEventRounds = useCallback(async () => {
    if (selectedEvent) {
      const resp = await getRounds(selectedEvent.event.id, {
        size: 1000,
        page: 0
      })
      setSelectedEvent({
        ...selectedEvent,
        rounds: resp.data.content
      })
    }
  }, [selectedEvent])

  return {
    events,
    loading,
    loadEvents,
    loadEvent,
    addEvents,
    selectedEvent,
    nextEvent,
    loadingTeams,
    selectedEventTeams,
    clearEvents,
    joinEvent,
    leaveEvent,
    loadSelectedEventTeams,
    addTeamToSelectedEvent,
    deleteTeamToSelectedEvent,
    refreshSelectedEventRounds,
    setSelectedEvent
  }
}

let Events = createContainer(useEventsHook)

export const EventsProvider = Events.Provider
export const useEvents = Events.useContainer
