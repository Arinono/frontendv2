import React, { useCallback, useEffect, useMemo, useState } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import Modal from '../Modal'
import Button from '../../ui/Button'
import UserSearch from '../search/UserSearch'
import Title from '../../ui/Title'
import { useEvents } from '../../store/events'
import { useUser } from '../../store/user'
import {
  getInvites,
  getMeTeamForEvent,
  getTeamInvites,
  inviteMember
} from '@breci/eventz_api'
import { useUsersInfo } from '../../store/usersInfo'
import UserInfo from '../userInfo'
import InvitationCard from '../InvitationCard'
import TeamInvitationCard from '../TeamInvitationCard'

const EventTeamsManagementModal = ({
  show,
  event,
  team = null,
  user,
  onClose
}) => {
  const { loadUsers, getUsers } = useUsersInfo()

  const hasTeam = useMemo(() => !!team, [team])
  const isTeamFull = useMemo(() => false, [])
  const isTeamLeader = useMemo(
    () => team && team.managerId === user.accountId,
    [team, user]
  )

  const [userToAdd, setUsersToAdd] = useState(null)
  const [newTeamName, setNewTeamName] = useState('')

  const { joinEvent, addTeamToSelectedEvent } = useEvents()
  const { addTeam } = useUser()

  const [teamInvites, setTeamsInvites] = useState([])
  const [userInvites, setUserInvites] = useState([])
  const [teamMembers, setTeamMembers] = useState([])

  const [usersInfoLoading, setUsersInfoLoading] = useState(false)

  // TODO add loading states!

  // little lazy hack
  const [refreshKey, setRefreshKey] = useState(Date.now())

  // TODO move all these API call in a dedicated event store
  // we are too short on time right now to do it
  const refresh = useCallback(() => {
    if (isTeamLeader) {
      getTeamInvites(event.id, { size: 1000, page: 0 }, 'AWAITING').then(
        async (res) => {
          if (res.data.content) {
            const invited = res.data.content.map((i) => i.accountId)
            const invitedUsers = await loadUsers(invited)
            setTeamsInvites(
              res.data.content.map((i) => ({
                ...i,
                invitedUser: invitedUsers.find(
                  (u) => u.accountId === i.accountId
                )
              }))
            )
          }
        }
      )
      setUserInvites([])
    } else {
      setTeamsInvites([])
      getInvites({ size: 1000, page: 0 }, 'AWAITING').then(async (res) => {
        if (res.data.content) {
          // TODO filter by event here
          const invitedBy = res.data.content.map((i) => i.invitedById)
          await loadUsers(invitedBy)
          setUserInvites(
            res.data.content
              .filter((i) => i.eventId === event.id)
              .map((i) => ({
                ...i,
                invitedBy: getUsers([i.invitedById])[0]
              }))
          )
        }
      })
    }
  }, [event])

  useEffect(() => {
    if (team) {
      setUsersInfoLoading(true)
      loadUsers(team.memberIds).then((info) => {
        setTeamMembers(info)
      })
    }
  }, [team])

  useEffect(() => {
    if (isTeamLeader) {
      getTeamInvites(event.id, { size: 1000, page: 0 }, 'AWAITING').then(
        async (res) => {
          if (res.data.content) {
            const invited = res.data.content.map((i) => i.accountId)
            const invitedUsers = await loadUsers(invited)
            setTeamsInvites(
              res.data.content.map((i) => ({
                ...i,
                invitedUser: invitedUsers.find(
                  (u) => u.accountId === i.accountId
                )
              }))
            )
          }
        }
      )
    } else {
      setTeamsInvites([])
    }
  }, [isTeamLeader])
  useEffect(() => {
    if (!isTeamLeader) {
      getInvites({ size: 1000, page: 0 }, 'AWAITING').then(async (res) => {
        if (res.data.content) {
          const invitedBy = res.data.content.map((i) => i.invitedById)
          const invitedByUsers = await loadUsers(invitedBy)
          // add user info
          setUserInvites(
            res.data.content
              .filter((i) => i.eventId === event.id)
              .map((i) => ({
                ...i,
                invitedBy: invitedByUsers.find(
                  (u) => u.accountId === i.invitedById
                )
              }))
          )
        }
      })
    } else {
      setUserInvites([])
    }
  }, [isTeamLeader])

  return (
    <Modal show={true} onClose={onClose} hideActions theme="dark" height="auto">
      <Header level={2}>Votre équipe</Header>
      <Content>
        <Section>
          {!hasTeam && (
            <div>
              <div>Vous n'avez pas rejoins l'évènement</div>
              <div>
                <Title level={3}>créer une équipe</Title>
                <TeamCreationInput>
                  <TeamNameInput
                    placeholder="Nom de l'équipe..."
                    value={newTeamName}
                    onInput={(e) => setNewTeamName(e.target.value)}
                  />
                  <Button
                    onClick={async () => {
                      // TODO send API request for team creation
                      const resp = await joinEvent(event.id, newTeamName)

                      addTeamToSelectedEvent(resp.data.team)
                      addTeam(resp.data.team)
                    }}
                  >
                    Créer
                  </Button>
                </TeamCreationInput>
              </div>
            </div>
          )}
          {hasTeam && (
            <div>
              <Title level={3}>{team.name}</Title>
              <div>
                {teamMembers.map((m) => (
                  <UserInfo
                    theme="dark"
                    userName={m.identity.username}
                    login={m.identity.login}
                    userLink={`https://www.twitch.tv/${m.identity.login}`}
                    userImage={m.identity.profileImageURL}
                    extended
                    key={m.id}
                  />
                ))}
              </div>
              {isTeamLeader && (
                <InvitedPlayersContainer>
                  <Title level={3}>Joueurs invités</Title>
                  {teamInvites.length <= 0 ? (
                    <Empty>Aucun joueur n'est invité</Empty>
                  ) : (
                    <InvitesList>
                      {teamInvites.map((i) => (
                        <TeamInvitationCard
                          target={i.invitedUser.identity.username}
                          targetLogin={i.invitedUser.identity.login}
                          image={i.invitedUser.identity.profileImageURL}
                          targetFollowers={i.invitedUser.identity.followers}
                          targetView={i.invitedUser.identity.views}
                          key={i.id}
                        />
                      ))}
                    </InvitesList>
                  )}

                  {!isTeamFull && (
                    <div>
                      <Title level={3}>Inviter un joueur</Title>

                      <SearchContainer>
                        <UserSearch
                          key={refreshKey}
                          onUserSelected={(user) => {
                            setUsersToAdd(user)
                          }}
                          value={userToAdd}
                        />
                        {userToAdd && (
                          <UserToAddContainer>
                            <Button
                              size="xs"
                              hollow
                              onClick={() => {
                                setUsersToAdd(null)
                                setRefreshKey(Date.now())
                              }}
                            >
                              Annuler
                            </Button>
                            <Button
                              size="xs"
                              onClick={async () => {
                                // TODO send invite
                                await inviteMember(event.id, {
                                  accountId: userToAdd.accountId
                                })

                                refresh()
                              }}
                            >
                              Inviter
                            </Button>
                          </UserToAddContainer>
                        )}
                      </SearchContainer>
                    </div>
                  )}
                </InvitedPlayersContainer>
              )}
            </div>
          )}
        </Section>
        {!hasTeam && (
          <Section>
            <Title level={3}>invitations</Title>
            <InvitesList>
              {userInvites.length > 0 &&
                userInvites.map((invite) => (
                  <InvitationCard
                    key={invite.id}
                    sender={invite.invitedBy.identity.username}
                  />
                ))}

              {userInvites.length === 0 && (
                <Empty>Vous n'avez pas d'invitations</Empty>
              )}
            </InvitesList>
          </Section>
        )}
      </Content>
      <Footer>
        <Button onClick={onClose}>Fermer</Button>
      </Footer>
    </Modal>
  )
}

const Empty = styled.div`
  ${tw`text-sm opacity-75 italic`}
`
const Header = styled(Title)``
const Content = styled.div``
const Footer = styled.div``
const Section = styled.div`
  ${tw`mb-4`}
`

const TeamNameInput = styled.input`
  ${tw`text-black pl-2 py-1 rounded mr-4 w-full`}
  &:focus,&:active {
    outline: none;
  }
`

const TeamCreationInput = styled.div`
  ${tw`flex`}
`

const SearchContainer = styled.div`
  input {
    ${tw`pl-2 py-1`}
    &:active,&:focus {
      outline: none;
    }
  }
`

const UserToAddContainer = styled.div`
  ${tw`inline-flex`}
`

const InvitesList = styled.div`
  ${tw`flex flex-col`}
  & >* {
    ${tw`my-2`}
  }
`

const InvitedPlayersContainer = styled.div`
  ${tw`flex flex-col justify-start`}
`

const InvitedPlayersAddInviteButton = styled(Button)`
  margin: unset;
`

export default EventTeamsManagementModal
