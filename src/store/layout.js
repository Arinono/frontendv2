import { useCallback, useMemo, useState } from 'react'
import { createContainer } from 'unstated-next'

function useLayoutHook() {
  const [subMenus, setSubMenus] = useState([])
  const showSubMenu = useMemo(() => subMenus.length > 0, [subMenus])

  const updateSubmenu = useCallback((items) => {
    setSubMenus(items)
  }, [])
  return { subMenus, showSubMenu, updateSubmenu }
}

let Layout = createContainer(useLayoutHook)

export const LayoutProvider = Layout.Provider
export const useLayout = Layout.useContainer
