import React from 'react'
import ReactDOM from 'react-dom'
import { LanguageProvider } from './store/language'
import { UserProvider } from './store/user'
import { EventsProvider } from './store/events'
import { UserInfoProvider } from './store/usersInfo'
import { TeamsProvider } from './store/teams'
import './app.css'
import './icons'
import App from './app'
import { setApiURL } from '@breci/eventz_api'
import config from './config'
import { initGA } from './ga'
import { initSentry } from './sentry'
import register from './sw'

register()

if (config.ENV === 'production') {
  initGA()
  initSentry(config.SENTRY_DSN, config.ENV)
}

setApiURL(config.API_URL)

ReactDOM.render(
  <React.StrictMode>
    <LanguageProvider>
      <EventsProvider>
        <UserInfoProvider>
          <UserProvider>
            <TeamsProvider>
              <App />
            </TeamsProvider>
          </UserProvider>
        </UserInfoProvider>
      </EventsProvider>
    </LanguageProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
