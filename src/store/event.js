import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { createContainer } from 'unstated-next'
import { unstable_batchedUpdates } from 'react-dom'
import {
  getMeTeamForEvent,
  getRanking,
  getRounds,
  getTeams
} from '@breci/eventz_api'
import { toDate, isAfter, isWithinInterval, isSameDay } from 'date-fns'
import { useEvents } from './events'
import { useTeams } from './teams'
import { useUsersInfo } from './usersInfo'
import config from '../config'

const ANONYMOUS_IDENTITY = {
  // default identity to have a placeholder
  profileImageURL: '',
  username: 'Anonyme',
  login: 'zerator',
  views: 0,
  followers: 0
}

function useEventHook(
  initialState = {
    eventId: ''
  }
) {
  const isInitialMount = useRef(true)

  const { events, loadEvent } = useEvents()
  const { loadUsers, getUser, getUserByAccountId } = useUsersInfo()
  const [event, setEvent] = useState(null)
  const [team, setTeam] = useState(null)
  const [rounds, setRounds] = useState([])
  const [teams, setTeams] = useState([])

  const [rankings, setRankings] = useState({})

  const meta = useMemo(() => {
    if (event) {
      let now = new Date()

      const startEventAtDate = toDate(event.startEventAt * 1000)
      const endEventAtDate = toDate(event.endEventAt * 1000)

      const openRegistrationsAtDate = toDate(event.openRegistrationsAt * 1000)
      const closeRegistrationsAtDate = toDate(event.closeRegistrationsAt * 1000)
      return {
        closeRegistrationsAtDate,
        endEventAtDate: toDate(event.endEventAt * 1000),
        openRegistrationsAtDate,
        startEventAtDate,
        isFuture: isAfter(startEventAtDate, now),
        isPast: isAfter(now, endEventAtDate),
        isLive: isWithinInterval(now, {
          start: startEventAtDate,
          end: endEventAtDate
        }),
        isRegistrationOpen: isWithinInterval(now, {
          start: openRegistrationsAtDate,
          end: closeRegistrationsAtDate
        }),
        lastOneDay: isSameDay(startEventAtDate, endEventAtDate),
        // Temporary placeholder for images

        images: {
          header: '',
          background: '',
          logo: ''
        }
      }
    }
    return null
  }, [event])

  const [eventLoading, setEventLoading] = useState(false)
  const [eventTeamsLoading, setEventTeamsLoading] = useState(false)
  const [eventMeTeamLoading, setEventMeTeamLoading] = useState(false)
  const [eventRoundsLoading, setEventRoundsLoading] = useState(false)
  const [eventUsersLoading, setEventUsersLoading] = useState(false)
  const [usersInfoLoading, setUsersInfoLoading] = useState(false)
  const [rankingLoading, setRankingLoading] = useState(false)

  const [eventInitialized, setEventInitialized] = useState(false)
  const [eventTeamsInitialized, setEventTeamsInitialized] = useState(false)
  const [eventMeTeamInitialized, setEventMeTeamInitialized] = useState(false)
  const [eventRoundsInitialized, setEventRoundsInitialized] = useState(false)
  const [eventUsersInitialized, setEventUsersInitialized] = useState(false)
  const [usersInfoInitialized, setUsersInfoInitialized] = useState(false)
  const [rankingInitialized, setRankingInitialized] = useState(false)

  const loading = useMemo(
    () =>
      eventLoading ||
      eventTeamsLoading ||
      eventMeTeamLoading ||
      eventRoundsLoading ||
      usersInfoLoading ||
      rankingLoading ||
      eventUsersLoading,
    [
      eventLoading,
      eventTeamsLoading,
      eventMeTeamLoading,
      eventRoundsLoading,
      usersInfoLoading,
      rankingLoading,
      eventUsersLoading
    ]
  )

  const reset = useCallback(() => {
    unstable_batchedUpdates(() => {
      setEventLoading(false)
      setEventTeamsLoading(false)
      setEventMeTeamLoading(false)
      setEventRoundsLoading(false)
      setEventUsersLoading(false)
      setUsersInfoLoading(false)
      setRankingLoading(false)
      setEventTeamsInitialized(false)
      setEventMeTeamInitialized(false)
      setEventRoundsInitialized(false)
      setEventUsersInitialized(false)
      setUsersInfoInitialized(false)
      setRankingInitialized(false)
      setEvent(null)
      setTeam(null)
      setRounds([])
      setTeams([])
    })
  }, [])
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false
    } else {
      // Your useEffect code here to be run on update
      reset()
    }
  }, [initialState.eventId])

  /*
   // load event when
  useEffect(() => {
    ;(async function t() {
      setEventLoading(true)

      let selectedEvent = events.find((e) => e.id === initialState.eventId)
      if (!selectedEvent) {
        const selectedEvent = await loadEvent(initialState.eventId)
      }
      setEvent(selectedEvent)
      setEventLoading(false)
    })()
  }, [initialState.eventId, events])
   */
  // load event when eventId change
  useEffect(() => {
    const t = async function t() {
      setEventLoading(true)

      let selectedEvent = events.find((e) => e.id === initialState.eventId)
      if (!selectedEvent) {
        selectedEvent = await loadEvent(initialState.eventId)
      }

      setEvent(selectedEvent)
      setEventInitialized(true)
      setEventLoading(false)
    }
    t()
  }, [initialState.eventId])

  const loadMeTeam = useCallback(async () => {
    setEventMeTeamLoading(true)
    // load me team and add it to global cache at the same time
    const t = await getMeTeamForEvent(initialState.eventId).catch((e) => {
      if (e.response) {
        if (e.response.status === 404) {
          // no teams
          return null
        }
      }
      throw e
    })
    setTeam(t)
    setEventMeTeamInitialized(true)
    setEventMeTeamLoading(false)

    setTeam(t)
  }, [initialState.eventId])

  const loadRankings = useCallback(async () => {
    setRankingLoading(true)

    const ranks = await Promise.all(
      rounds.map((r) =>
        getRanking(r.id, {
          size: 1000,
          page: 0
        }).then((resp) => resp.data.content)
      )
    )

    setRankings(
      ranks.reduce((reducer, rankToAdd) => {
        if (rankToAdd.length) {
          return {
            ...reducer,
            [rankToAdd[0].roundId]: rankToAdd
          }
        }
        return reducer
      }, {})
    )
    setRankingInitialized(true)
    setRankingLoading(false)
  }, [rounds])

  const loadTeams = useCallback(async () => {
    setEventTeamsLoading(true)
    const eventTeams = (
      await getTeams(
        {
          size: 1000,
          page: 0
        },
        initialState.eventId
      )
    ).data.content
    setTeams(eventTeams)
    setEventTeamsInitialized(true)
    setEventTeamsLoading(false)
  }, [])

  const loadRounds = useCallback(async () => {
    setEventRoundsLoading(true)
    const res = await getRounds(initialState.eventId, {
      size: 1000,
      page: 0
    })

    const rounds = res.data.content

    setRounds(rounds)
    setEventRoundsLoading(false)
    setEventRoundsInitialized(true)
  }, [initialState.eventId])

  const loadUserDataFromRounds = useCallback(async () => {
    setUsersInfoLoading(true)
    if (teams.length) {
      const users = await loadUsers(teams.map((team) => team.memberIds).flat())
    }
    setUsersInfoInitialized(true)
    setUsersInfoLoading(false)
  }, [teams])

  const addTeamToEvent = (team) => {
    setTeams((t) => [...t, team])
  }

  const removeTeamFromEvent = (teamToRemove) => {
    setTeams((t) => {
      const i = t.findIndex((team) => team.id === teamToRemove.id)
      if (i > -1) {
        const newTeams = [...t]
        newTeams.splice(i, 1)
        return newTeams
      }
      return t
    })
  }

  const joinEvent = () => {}
  const leaveEvent = () => {}

  const inviteUserToTeam = () => {}

  const extendedTeamsWithUsersInfo = useMemo(() => {
    if (usersInfoInitialized) {
      return teams.map((t) => {
        return {
          ...t,
          members: t.memberIds.map(
            (m) =>
              getUserByAccountId(m) || {
                id: m.id,
                identity: ANONYMOUS_IDENTITY
              }
          )
        }
      })
    }
    return null
  }, [teams, usersInfoInitialized, getUser])

  const extendedRankingsWithUsersInfo = useMemo(() => {
    // TODO change from array to object
    if (rankingInitialized && rankings && usersInfoInitialized) {
      return Object.values(rankings).reduce((reducer, ranking) => {
        return {
          ...reducer,
          [ranking[0].roundId]: ranking.map((r) => {
            return {
              ...r,
              team: {
                ...r.team,
                members: r.team.members.map(
                  (m) =>
                    getUserByAccountId(m) || {
                      id: m.id,
                      identity: ANONYMOUS_IDENTITY
                    }
                )
              }
            }
          })
        }
      }, {})
    }
    return null
  }, [rankings, rankingInitialized, usersInfoInitialized])

  const extendedRoundWithRankingsAndUserInfo = useMemo(() => {
    if (rounds && extendedRankingsWithUsersInfo && usersInfoInitialized) {
      return rounds.map((round) => {
        return {
          ...round,
          rankings: extendedRankingsWithUsersInfo[round.id] || []
        }
      })
    }
  }, [rounds, extendedRankingsWithUsersInfo, usersInfoInitialized])
  return {
    event,
    meta,
    rounds,
    teams,
    team,
    rankings,

    extendedTeamsWithUsersInfo,
    extendedRankingsWithUsersInfo,
    extendedRoundWithRankingsAndUserInfo,

    loadEvent,
    loadMeTeam,
    loadRounds,
    loadTeams,
    loadUserDataFromRounds,
    loadRankings,

    eventInitialized,
    usersInfoInitialized,
    eventRoundsInitialized,
    eventMeTeamInitialized,
    eventTeamsInitialized,
    eventUsersInitialized,
    rankingInitialized,

    joinEvent,
    leaveEvent
  }
}

let Event = createContainer(useEventHook)

export const EventProvider = Event.Provider
export const useEvent = Event.useContainer
