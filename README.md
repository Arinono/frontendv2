# *FFS Frontend v2*

**FightForSub is powered by Well-Played.gg, a SaaS platform to manage Events & Tournaments**

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/fightforsub/frontendv2?style=plastic)](https://gitlab.com/fightforsub/frontendv2/builds)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

[![Discord](https://img.shields.io/discord/910188375963742240)](https://discord.gg/pRJxq6HK9M)
[![Last release](https://img.shields.io/gitlab/v/release/fightforsub/frontendv2)](https://gitlab.com/fightforsub/frontendv2/-/releases)

V2 Frontend of the FightForSub's ZeratoR Project

**The project is under active development.**

## Table of Contents

- [Installation and Configuration](#installation-and-configuration)
- [Usage](#usage)
- [Contributing](#contributing)
  - [Well-Played.gg API Documentation](#api-documentation)
  - [Architecture](#architecture)
- [Contact](#contact)
  - [Troubleshooting and Support](#troubleshooting-and-support)
- [Credits and Acknowledgments](#credits-and-acknowledgments)
- [Copyright and Licensing](#copyright-and-licensing)
- [Changelog and News](#changelog-and-news)
- [Notes and References](#notes-and-references)
  - [Dependencies](#dependencies)

## Installation and Configuration

Run `npm i` or `yarn install`

## Usage

### npm start

Runs the app in the development mode.
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm run build

Builds a static copy of the website to the `build/` folder.
Can be deployed after that

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

## Api Documentation

Well-Played.gg's API documentation can be found [https://api.stg.well-played.gg/swagger-ui.html](here). Any questions regarding to this API should be redirected to [AlexMog](mailto:contact@mog.gg).

### Architecture

#### UI Elements

Path: `src/ui`

UI Elements are the most simple components

UI Elements can't use any plugin, store, translation.

#### Components

Path :`src/components`

Components are one or multiple UI elements puts together,

Components can use the translations in them, but not be connected to the store

#### Views

Path :`src/views`

Views are one or multiple components puts together,

Views can be connected to the store

#### Pages

Path :`src/pages`

Pages are components that will use views and components and UI elements to be used as a final page, they wil be displayed inside a layout.

#### Layouts

Path :`src/layouts`

Layouts are top layer components, they are used to define common organisation of pages.

Layouts can have i18n, store access, etc

## Contact

- FightForSub global management: @AlexMog <contact@mog.gg>
- Initial V2 developers: Brice Culas - @briceculas, Grégoire Daussin - @Azaret

### Troubleshooting and Support

Consider using [Issues](https://gitlab.com/fightforsub/frontendv2/-/issues) for your support or feature requests.

## Credits and Acknowledgments

- [AlexMog](https://twitter.com/alexmog_fr) - FightForSub global management
- Brice Culas - @briceculas - v2 initial author
- Grégoire Daussin - @Azaret - v2 initial dev

## Copyright and Licensing

- [MIT](LICENSE)

## Changelog and News

- [Changelog](../CHANGELOG.md)

## Notes and References

### Dependencies

- [NodeJS](https://nodejs.org/en/) - Programming language engine
