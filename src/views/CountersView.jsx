import React, { useState } from 'react'
import Counter from '../components/Counter'
import { useCounter } from '../store/count'

const CounterView = () => {
  const [value1, setValue1] = useState(0)
  const [value2, setValue2] = useState(0)

  const count = useCounter()
  return (
    <div>
      <div>
        Default counters
        <Counter
          value={value1}
          onIncrease={() => setValue1(value1 + 1)}
          onDecrease={() => setValue1(value1 - 1)}
        />
        <Counter
          value={value2}
          onIncrease={() => setValue2(value2 + 1)}
          onDecrease={() => setValue2(value2 - 1)}
        />
      </div>
      <div>
        connected counters
        <Counter
          value={count.count}
          onIncrease={count.increment}
          onDecrease={count.decrement}
        />
        <Counter
          value={count.count}
          onIncrease={count.increment}
          onDecrease={count.decrement}
        />
      </div>
    </div>
  )
}

export default CounterView
