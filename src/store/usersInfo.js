import { useCallback, useState } from 'react'
import { createContainer } from 'unstated-next'
import {
  getIdentityForAccount,
  getIdentityForAccount_1
} from '@breci/eventz_api'

function useUsersInfoHook(initialState = 0) {
  const [users, setUsers] = useState([])

  const unloadUser = useCallback(
    (id) => {
      const userIndex = users.findIndex((u) => u.id === id)
      if (userIndex !== -1) {
        const newData = [...users].splice(userIndex, 1)
        setUsers(newData)
      }
    },
    [users]
  )

  const reloadUser = useCallback(
    async (id) => {
      const userIndex = users.findIndex((u) => u.id === id)
      if (userIndex !== -1) {
        const resp = getIdentityForAccount_1(id, 'TWITCH')
        const user = resp.data
        const newData = [...users].splice(userIndex, 1, user)
        setUsers(newData)
      }
    },
    [users]
  )

  const loadUsers = useCallback(
    /**
     *
     * @param ids {string[]}
     * @param force {boolean} Force the reload of all the users
     * @returns {Promise<Array>}
     */
    async (ids, force = false) => {
      const cleanedIds = force
        ? ids
        : ids.filter((id) => !users.find((u) => u.id === id))

      const resp = await getIdentityForAccount('TWITCH', cleanedIds, {
        size: 1000,
        page: 0
      })
      const newUsers = resp.data.content

      const previousUsers = users.filter((u) =>
        newUsers.find((_u) => u.accountId !== _u.accountId)
      )

      const finalUsers = [...newUsers, ...previousUsers]

      setUsers(finalUsers)

      return finalUsers.filter((u) => ids.includes(u.accountId))
    },
    [users]
  )

  const getUser = useCallback(
    (id) => {
      return users.find((user) => user.id === id)
    },
    [users]
  )
  const getUserByAccountId = useCallback(
    (id) => {
      return users.find((user) => user.accountId === id)
    },
    [users]
  )

  const getUsers = useCallback(
    (ids) => {
      return users.filter((user) => ids.includes(user.accountId))
    },
    [users]
  )

  return {
    unloadUser,
    reloadUser,
    loadUsers,
    getUser,
    getUsers,
    getUserByAccountId
  }
}

let UsersInfo = createContainer(useUsersInfoHook)

export const UserInfoProvider = UsersInfo.Provider
export const useUsersInfo = UsersInfo.useContainer
