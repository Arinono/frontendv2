import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import Button from '../ui/Button'

const Modal = ({
  show,
  message,
  cancelButtonText,
  onClose,
  confirmButtonText,
  onConfirm,
  children,
  hideActions,
  theme = 'light',
  height = 'auto'
}) => {
  return (
    <Container show={show}>
      <Dialog theme={theme} height={height}>
        {message ? (
          <Message>{message}</Message>
        ) : (
          <Content theme={theme}>{children}</Content>
        )}
        {!hideActions && (
          <Actions>
            <StyledButton onClick={onClose} hollow>
              {cancelButtonText}
            </StyledButton>
            <StyledButton onClick={onConfirm}>{confirmButtonText}</StyledButton>
          </Actions>
        )}
      </Dialog>
    </Container>
  )
}

const StyledButton = styled(Button)`
  ${tw`ml-4 mr-0`}
`

const Actions = styled.div`
  ${tw`flex flex-row justify-end`}
`

const Message = styled.div`
  ${tw`flex-grow text-lg text-black px-4 pt-3`}
  &::first-letter {
    ${tw`uppercase`}
  }
`

const Content = styled.div`
  ${tw`flex-grow text-lg text-black px-4 pt-3`}
  ${(props) => {
    if (props.theme === 'dark') {
      return tw`text-white`
    }
  }}
`

const Dialog = styled.div`
  ${tw`m-auto bg-white rounded-md w-4/6 md:w-4/5 md:max-w-screen-md p-4 flex flex-col`}

  ${(props) => {
    if (props.height !== 'auto') {
      return tw`h-64`
    }
  }}
  ${(props) => {
    if (props.theme === 'dark') {
      return tw`bg-black`
    }
  }}
`

const Container = styled.div`
  ${tw`fixed top-0 left-0 inset-0 z-50 p-4 flex flex-row`}
  height:100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.5);
  ${(props) => props.show !== true && tw`hidden`}
`

export default Modal
