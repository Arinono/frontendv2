import React, { useEffect, useMemo, useRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import { Link } from 'react-router-dom'
import { useRouteMatch } from 'react-router-dom'
import CrownIcon from '../ui/Icons/CrownIcon'
import Logo from '../ui/Logo'
import default_background from '../assets/background.svg'
import home_background from '../assets/home_background_v2.png'
import { AnimatePresence, motion } from 'framer-motion'
import { useUser } from '../store/user'
import { useEvents } from '../store/events'
import love from '../assets/love.png'
import { useNotifications } from '../store/notification'
import Notification from '../components/Notification'
import config from '../config'
import { useMediaQuery } from '@react-hook/media-query'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import Button from '../ui/Button'
import Dropdown from '../components/Dropdown'
import { useLayout } from '../store/layout'

function useOnClickOutside(ref, handler) {
  useEffect(
    () => {
      const listener = (event) => {
        // Do nothing if clicking ref's element or descendent elements
        if (!ref.current || ref.current.contains(event.target)) {
          return
        }

        handler(event)
      }

      document.addEventListener('mousedown', listener)
      document.addEventListener('touchstart', listener)

      return () => {
        document.removeEventListener('mousedown', listener)
        document.removeEventListener('touchstart', listener)
      }
    },
    // Add ref and handler to effect dependencies
    // It's worth noting that because passed in handler is a new ...
    // ... function on every render that will cause this effect ...
    // ... callback/cleanup to run every render. It's not a big deal ...
    // ... but to optimize you can wrap handler in useCallback before ...
    // ... passing it into this hook.
    [ref, handler]
  )
}

const BaseLayout = ({ children, ...restProps }) => {
  const isMobile = !useMediaQuery('(min-width: 640px)')
  const matchHome = useRouteMatch('/')
  const matchDirect = useRouteMatch('/direct')
  const matchEvenement = useRouteMatch('/evenement')

  const { showSubMenu, subMenus } = useLayout()

  const mainMenuRef = useRef()

  const [showMobileHeader, setShowMobileHeader] = useState(false)

  useOnClickOutside(mainMenuRef, () => setShowMobileHeader(false))

  const { notifications, removeNotification } = useNotifications()

  const { user, logIn, logOff } = useUser()
  const { selectedEvent, nextEvent } = useEvents()

  const showSubHeader = useMemo(
    () => (matchDirect || matchEvenement) && selectedEvent,
    [matchDirect, matchEvenement]
  )

  const showMainHeader = useMemo(
    () => (isMobile && showMobileHeader) || !isMobile,
    [isMobile, showMobileHeader]
  )

  const loggedIn = !!user

  const urlLogin = `${config.AUTH_URL}/login?organisation=${config.API_ORGANISATION_ID}&response_type=token&client_id=${config.API_CLIENT_ID}&redirect_uri=${window.location.origin}/auth/api&scope=`

  const isCurrentEventStarted = useMemo(
    () =>
      selectedEvent &&
      selectedEvent.startEventAt &&
      selectedEvent.startEventAt * 1000 < Date.now(),
    [selectedEvent]
  )

  return (
    <Container {...restProps} home={matchHome.isExact.toString()}>
      <NotificationContainer>
        <AnimatePresence>
          {notifications.map((item) => (
            <MovableNotification
              key={item.id}
              exit={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              initial={{ opacity: 0 }}
              layout
              positionTransition={{
                type: 'spring',
                stiffness: 350,
                damping: 25
              }}
            >
              <Notification
                notification={item}
                onClose={() => {
                  removeNotification(item.id)
                }}
              />
            </MovableNotification>
          ))}
        </AnimatePresence>
      </NotificationContainer>
      <Header>
        <MainHeader showSubHeader={showSubHeader} ref={mainMenuRef}>
          <MainHeaderContainer>
            {isMobile ? (
              <ButtonIcon
                onClick={() => {
                  setShowMobileHeader(false)
                }}
              >
                <FontAwesomeIcon icon={faTimes} />
              </ButtonIcon>
            ) : (
              <LogoContainer to="/">
                <motion.div layoutId="logo">
                  <Logo />
                </motion.div>
              </LogoContainer>
            )}
            {nextEvent && (
              <LiveLink to="/direct">
                <FormattedMessage
                  id={nextEvent.isLive ? 'menu.live_link' : 'menu.open_link'}
                />
                <LiveEvent>{nextEvent.name}</LiveEvent>
              </LiveLink>
            )}

            <MenuLink to="/evenements">
              <FormattedMessage id="menu.events_link" />
            </MenuLink>
            <MenuLink to="/a-propos">
              <FormattedMessage id="menu.about_link" />
            </MenuLink>
            <LoginMenu>
              {loggedIn ? (
                <Dropdown
                  options={[
                    {
                      render: <Link to="/profil">Profil</Link>
                    },
                    {
                      render: <FormattedMessage id="menu.logout_link" />,
                      onClick: logOff
                    }
                  ]}
                >
                  <MenuSpan>
                    <StyledCrownIcon />
                    {user.identity.username}
                  </MenuSpan>
                </Dropdown>
              ) : (
                <MenuA
                  href={urlLogin}
                  onClick={logIn}
                >
                  <FormattedMessage id="menu.login_link" />
                </MenuA>
              )}
            </LoginMenu>
          </MainHeaderContainer>
        </MainHeader>
        {showSubMenu && (
          <SubHeader>
            <SubHeaderContainer>
              {!isMobile && <LogoContainer />}
              {subMenus.map((item, index) => {
                return (
                  <MenuLink key={index} to={item.to}>
                    {isMobile ? (
                      <FontAwesomeIcon icon={item.icon} />
                    ) : (
                      <FormattedMessage id={item.labelKey} />
                    )}
                  </MenuLink>
                )
              })}
            </SubHeaderContainer>
          </SubHeader>
        )}
      </Header>
      {!matchHome.isExact && isMobile && !showMobileHeader && (
        <OpenSideMenu>
          <ButtonIcon onClick={() => setShowMobileHeader(true)}>
            <FontAwesomeIcon icon={faBars} />
          </ButtonIcon>
        </OpenSideMenu>
      )}
      {!matchHome.isExact && isMobile && (
        <MobileLogoContainer to="/">
          <motion.div layoutId="logo">
            <Logo />
          </motion.div>
        </MobileLogoContainer>
      )}
      <Content>{children}</Content>
      <Footer>
        <Credit to="/a-propos" home={matchHome.isExact.toString()}>
          <FormattedMessage id="credits.contact" />
          &nbsp;&middot;&nbsp;
          <FormattedMessage id="credits.legal" />
          &nbsp;&middot;&nbsp;
          <FormattedMessage
            id="credits.craft"
            values={{
              love: <LoveIcon src={love} alt="giusLove" />
            }}
          />
        </Credit>
      </Footer>
    </Container>
  )
}
const MovableNotification = styled(motion.li)`
  width: 50%;
  max-width: 36rem;
  list-style-type: none;
  ${tw`shadow mb-1`}
`

const NotificationContainer = styled(motion.div)`
  top: 0;
  left: 50%;
  position: fixed;
  z-index: 100;
  width: 100%;
  transform: translateX(-50%);
  ${tw`flex flex-col items-center`}
`

const Credit = styled(Link)`
  ${tw`w-full h-12 flex items-center text-white justify-center text-sm`}
  ${(props) => (props.home ? tw`bg-transparent` : `background-color: #113942;`)}
`

const LoveIcon = styled.img`
  ${tw`w-5 mx-2`}
`

const Footer = styled.div`
  ${tw`w-full flex flex-col`}
`

/**
 * Main Header
 * - Adding bottom margin to main header in case the sub header is not shown,
 * so page content top position is constant whenever the sub header menu is
 * shown or not
 */
const MainHeaderContainer = styled.div`
  ${tw`bg-primary sm:bg-transparent w-full container flex text-right sm:text-left items-end sm:items-start flex-col sm:flex-row sm:h-unset`}
  height: 100vh;
  ${(props) => !props.showSubHeader && tw`mb-15`}
`

const MainHeader = styled.div`
  ${tw`bg-transparent sm:bg-primary sm:w-full h-15 text-white text-sm flex justify-center fixed top-0 right-0 sm:static z-10`}
  ${(props) => !props.showSubHeader && tw`mb-15`}
`

const MobileLogoContainer = styled(Link)`
  ${tw`absolute top-0 left-0 pl-2 w-20 z-40 ml-1 my-4 sm:m-4`}
`

const LogoContainer = styled(Link)`
  ${tw`w-40 z-40 ml-1 my-4 sm:m-4`}
`

const MenuLink = styled(Link)`
  ${tw`h-15 pr-3 pl-3 leading-15 uppercase`}
`

const LoginMenu = styled.div`
  ${tw`flex-grow text-right`}
`

const MenuA = styled.a`
  ${tw`h-15 pr-3 pl-3 leading-15 uppercase`}
`
const MenuSpan = styled.span`
  ${tw`h-15 pr-3 pl-3 leading-15 uppercase`}
`

const StyledCrownIcon = styled(CrownIcon)`
  ${tw`fill-white w-6 inline-block mx-2 align-text-top`}
  margin-top: 0.0625rem;
`

const SubHeaderContainer = styled.div`
  ${tw`flex items-center container justify-center sm:justify-start`}
  ${MenuLink} {
    ${tw`text-base font-semibold`}
  }
`
const SubHeader = styled.div`
  ${tw`bg-white sm:bg-gray-lighter w-full h-15 flex justify-center text-black fixed sm:bottom-auto sm:static z-20`}
  bottom:0;
`

const LiveLink = styled(MenuLink)`
  ${tw`leading-none pt-4`}
`

const LiveEvent = styled.div`
  ${tw`leading-none text-xs font-thin opacity-75`}
`

const Header = styled(motion.div)`
  ${tw`absolute top-0 left-0 w-full`}
`

/**
 * Main Container
 * - For the background to show on all the page height, the height need to be
 * set to auto, with min-height to full to avoid white space at the bottom. At
 * the same time the height need to be set to full on the home page for the
 * content take all the height
 */
const Container = styled.div`
  ${tw`w-full min-h-full flex flex-col`}
  ${(props) =>
    props.home
      ? tw`bg-center bg-cover bg-gray-dark`
      : tw`bg-left-top bg-cyan-dark`}
  ${(props) =>
    props.home
      ? `
  background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)),
    url(${home_background});
  `
      : `
  background-image: url(${default_background});
  background-blend-mode: soft-light;
  `}
`

const Content = styled.div`
  ${tw`w-full flex-1`}
`

const ButtonIcon = styled.button`
  ${tw` p-4`}
`

const OpenSideMenu = styled.div`
  ${tw`absolute top-0 right-0 text-white`}
`

export default BaseLayout
