import { FormattedMessage } from 'react-intl'
import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import { motion } from 'framer-motion'

const LiveTag = ({ ...restProps }) => (
  <Live {...restProps}>
    <Dot
      animate={{ opacity: [0, 1, 0] }}
      transition={{
        loop: Infinity,
        duration: 3
      }}
    />
    <FormattedMessage id="events.live" />
  </Live>
)

const Live = styled.div`
  ${tw`absolute right-0 top-0 bg-black pl-1 pr-6 capitalize rounded m-2`}
`

const Dot = styled(motion.div)`
  ${tw`absolute right-0 top-0 bg-red h-2 w-2 rounded-full`}
  margin: 0.45rem;
`

export default LiveTag
