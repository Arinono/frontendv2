import Card from '../ui/Card'
import Button from '../ui/Button'
import React, { useMemo } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import UserInfo from './userInfo'

const TeamInvitationCard = ({
  image,
  target,
  targetFollowers = 0,
  targetView = 0,
  targetLogin = '',
  onCancel
}) => {
  return (
    <CardContainer>
      <Text>
        <UserInfo
          userName={target}
          views={targetView}
          followers={targetFollowers}
          userImage={image}
          extended
          theme="dark"
          userLink={`https://www.twitch.tv/${targetLogin}`}
        />
      </Text>
      <Actions>
        <Button size="xs" hollow>
          Annuler
        </Button>
      </Actions>
    </CardContainer>
  )
}

const CardContainer = styled(Card)`
  ${tw`flex flex-row p-2 pl-4`}
`

const Text = styled.div`
  ${tw`flex-1`}
`

const Actions = styled.div`
  ${tw`flex flex-row`}
  & > * {
    ${tw`mx-2`}
  }
`

const Media = styled.img``

export default TeamInvitationCard
