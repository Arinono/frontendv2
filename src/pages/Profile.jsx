import React, { useEffect } from 'react'
import tw from 'twin.macro'
import styled from 'styled-components'
import Card from '../ui/Card'
import { useUser } from '../store/user'
import ContentPage from '../ui/ContentPage'
import Media from '../ui/Media'
import Title from '../ui/Title'
import { useEvents } from '../store/events'
import { useTeams } from '../store/teams'

const ProfilePage = () => {
  const { user, tokenInitialized } = useUser()
  const events = useEvents()
  const {
    loading,
    teams = [],
    pendingInvites,
    acceptedInvites,
    loadMeTeams,
    loadAcceptedInvites,
    loadPendingInvites
  } = useTeams()
  useEffect(() => {
    if (tokenInitialized) {
      loadMeTeams()
      loadAcceptedInvites()
      loadPendingInvites()
    }
  }, [tokenInitialized])

  return (
    <ContentPage>
      <Card className="w-full">
        {user && (
          <div>
            <Header>
              <StyledMedia src={user.identity.profileImageURL} shape="circle" />
              <Username>{user.identity.username}</Username>
            </Header>
            <Content>
              <Section>
                {events && (
                  <Section>
                    <Title level={2}>Events à venir</Title>
                    <EmptyBlock>
                      Vous n'avez pas d'évènements à venir
                    </EmptyBlock>

                    <div>hahah</div>
                    {JSON.stringify(teams)}
                  </Section>
                )}
                <Section>
                  <Title level={3}>Events passés</Title>
                  <EmptyBlock>
                    Vous n'avez participé à aucun évènements
                  </EmptyBlock>
                </Section>
              </Section>
              <Section>
                <Section>
                  <Title level={2}>Equipes</Title>
                  {acceptedInvites.length === 0 ? (
                    <EmptyBlock> Vous n'êtes dans aucune équipe</EmptyBlock>
                  ) : (
                    <div></div>
                  )}
                </Section>

                <Section>
                  <Title level={3}>Invitations</Title>
                  {pendingInvites.length === 0 ? (
                    <EmptyBlock>Vous n'avez pas d'invitations</EmptyBlock>
                  ) : (
                    <div></div>
                  )}
                </Section>
              </Section>
            </Content>
          </div>
        )}
      </Card>
    </ContentPage>
  )
}

const StyledMedia = styled(Media)`
  ${tw`w-24 h-24 mr-4`}
`

const Header = styled.div`
  ${tw`flex items-center flex-col`}
`

const Username = styled(Title)`
  ${tw`pt-2`}
`

const EmptyBlock = styled.div`
  ${tw`opacity-75`}
`

const Section = styled.div`
  ${tw`mb-4`}
`

const Content = styled.div`
  ${tw`flex`}
  &>* {
    width: 50%;
  }
`

export default ProfilePage
