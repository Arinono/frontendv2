const isProd = window.location.origin === process.env.REACT_APP_PROD_URL

const config = isProd
  ? {
      ENV: 'production',
      API_URL: process.env.REACT_APP_API_URL_PROD,
      AUTH_URL: process.env.REACT_APP_AUTH_URL_PROD,
      API_CLIENT_ID: process.env.REACT_APP_API_CLIENT_ID_PROD,
      API_CLIENT_ID_FIX: process.env.REACT_APP_API_CLIENT_ID_FIX_PROD,
      API_ORGANISATION_ID: process.env.REACT_APP_API_ORGANISATION_ID_PROD,
      SENTRY_DSN: process.env.REACT_APP_SENTRY_DSN
    }
  : {
      ENV: 'development',
      API_URL: process.env.REACT_APP_API_URL,
      AUTH_URL: process.env.REACT_APP_AUTH_URL,
      API_CLIENT_ID: process.env.REACT_APP_API_CLIENT_ID,
      API_CLIENT_ID_FIX: process.env.REACT_APP_CLIENT_ID_FIX,
      API_ORGANISATION_ID: process.env.REACT_APP_API_ORGANISATION_ID
    }

export default config
