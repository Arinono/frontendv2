import styled from 'styled-components'
import tw from 'twin.macro'

/**
 * Wrap the animated component to hide the overflow
 * @type {*|{after: string, type: string, value: string}}
 */
export const OverflowHiddenWrapper = styled.div`
  ${({ overflowHidden }) => {
    switch (overflowHidden) {
      case 'both':
        return tw`overflow-hidden`
      case 'x':
        return tw`overflow-x-hidden`
      case 'y':
        return tw`overflow-y-hidden`
    }
  }}
`

OverflowHiddenWrapper.defaultProps = {
  overflowHidden: 'both'
}
