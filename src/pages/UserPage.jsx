import React from 'react'
import styled from 'styled-components'

function UserPage() {
  return <Container>User Page</Container>
}

const Container = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

export default UserPage
