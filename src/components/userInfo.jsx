import React, { useMemo } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import Media from '../ui/Media'
import Icon from '../ui/Icons/Icon'
import { useMediaQuery } from '@react-hook/media-query'

function formatNumberToString(number) {
  if (!number) {
    return 0
  }
  if (number > 1000000) {
    return `${Math.round(number / 1000000)}M`
  } else if (number > 1000) {
    return `${Math.round(number / 1000)}K`
  } else {
    return number.toString()
  }
}

const UserInfo = ({
  userName,
  login,
  userLink,
  userImage,
  views,
  followers,
  theme = 'light',
  extended = false
}) => {
  const isMobile = !useMediaQuery('(min-width: 640px)')

  const formattedView = useMemo(() => formatNumberToString(views), [views])
  const formattedFollower = useMemo(
    () => formatNumberToString(followers),
    [followers]
  )

  return (
    <Container theme={theme}>
      {extended && <StyledMedia src={userImage} shape="circle" />}
      <Info>
        <Name>{userName}</Name>
        {!isMobile && (
          <Link href={userLink} target="_blank">
            twitch.tv/{login}
          </Link>
        )}
        {extended && (
          <Stats>
            <StatsField>
              <Icon icon="eye" />
              {formattedView}
            </StatsField>
            <StatsField>
              <Icon icon="people" />
              {formattedFollower}
            </StatsField>
          </Stats>
        )}
      </Info>
    </Container>
  )
}

const Name = styled.span`
  ${tw`text-base sm:text-lg mb-1`}
`
const Link = styled.a`
  ${tw`text-xs sm:text-sm text-green mb-1`}
`
const StatsField = styled.div`
  ${tw`flex text-xs sm:text-sm`}

  &:not(:last-child) {
    ${tw`mr-2`}
  }
  svg {
    ${tw`flex mr-1`}
  }
`
const Stats = styled.div`
  ${tw`opacity-75 flex`}

  svg {
    ${tw`fill-current w-4`}
  }
`

const Info = styled.div`
  ${tw`flex flex-col`}
`

const StyledMedia = styled(Media)`
  ${tw`w-12 h-12 mr-4`}
`
const Container = styled.div`
  ${tw`flex items-center`}
  ${(props) => (props.theme === 'light' ? tw`text-black` : tw`text-white`)}
`

export default UserInfo
