import styled from 'styled-components'
import tw from 'twin.macro'

const Card = styled.div`
  ${tw`bg-black-dark px-8 py-6 rounded shadow inline-block text-white`}
`

export default Card
