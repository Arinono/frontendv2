// apiRounds is an array of round data each round data is also an array
/*
{
    rank: 1,
    total: 100,
    user: {
      name: 'breci',
      id: '1',
      link: 'https://www.twitch.tv/breci',
      img:
        'https://static-cdn.jtvnw.net/jtv_user_pictures/breci-profile_image-218b2173f80d1815-300x300.jpeg'
    },
    scores: [0, 60, 40]
  }
 */
export function convertApiRoundToDisplayRound(apiRounds, roundIdFilter) {
  if (roundIdFilter) {
  }

  return apiRounds.reduce((reducer, round) => {
    const formattedData = round.rankings.reduce((r, roundValue) => {
      if (roundIdFilter && roundValue.roundId !== roundIdFilter) return r

      const id = roundValue.teamId
      return {
        ...r,
        [id]: {
          ...(r[id] | {}),
          scores: r[id]
            ? [...r[id].scores, roundValue.score]
            : [roundValue.score],
          total: (r[id] ? r[id].total : 0) + roundValue.score
        }
      }
    }, reducer)

    return formattedData
  }, {})
}
