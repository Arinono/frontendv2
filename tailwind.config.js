const GREEN = '#43a81d'
const DARK_GREEN = '#467F34'
const RED = '#e92c2c'
const BLUE = '#2DBEE8'
const DARK_BLUE = '#1D9FC4'
const DARK_CYAN = '#113942'
const WHITE = '#ffffff'
const BLACK = '#333333'
const DARK_BLACK = '#141715'
const LIGHTER_GRAY = '#E5E5E5'
const LIGHT_GRAY = '#E3E8E2'
const GRAY = '#828282'
const DARK_GRAY = '#292929'
const DARKER_GRAY = '#212121'
const SILVER = '#D6D6D6'
const BRONZE = '#A16A6A'

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  theme: {
    typography: {
      default: {
        css: {
          fontSize: '0.75rem',
          color: WHITE,
          '[class~="lead"]': {
            color: WHITE
          },
          p: {
            marginBottom: '0.5em',
            marginTop: '0.5em'
          },
          a: {
            color: WHITE
          },
          strong: {
            color: WHITE
          },
          ul: {
            marginBottom: '0.75em',
            marginTop: '0.75em'
          },
          ol: {
            marginBottom: '0.75em',
            marginTop: '0.75em'
          },
          'ol > li::before': {
            color: WHITE
          },
          'ul > li::before': {
            backgroundColor: GREEN
          },
          ' ul > li > *:first-child': {
            marginBottom: '0.75em',
            marginTop: '0.75em'
          },
          ' ul > li > *:last-child': {
            marginBottom: '0.75em',
            marginTop: '0.75em'
          },
          hr: {
            borderColor: GREEN
          },
          blockquote: {
            color: WHITE,
            borderLeftColor: GREEN
          },
          h1: {
            color: WHITE,
            marginBottom: '0.5em'
          },
          h2: {
            color: WHITE,
            marginTop: '1em',
            marginBottom: '0.5em'
          },
          h3: {
            color: WHITE
          },
          h4: {
            color: WHITE
          },
          'figure figcaption': {
            color: WHITE
          },
          code: {
            color: WHITE
          },
          pre: {
            color: WHITE,
            backgroundColor: DARK_GRAY,
            background: DARK_GRAY,
            marginBottom: '1em',
            marginTop: '1em'
          },
          thead: {
            color: WHITE,
            fontWeight: '600',
            borderBottomWidth: '1px',
            borderBottomColor: GREEN
          },
          'tbody tr': {
            borderBottomWidth: '1px',
            borderBottomColor: GREEN
          }
        }
      }
    },
    extend: {
      spacing: {
        unset: 'unset'
      },
      colors: {
        primary: {
          DEFAULT: GREEN,
          dark: DARK_GREEN
        },
        secondary: {
          DEFAULT: BLUE,
          dark: DARK_BLUE
        }
      },
      inset: {
        '1/2': '50%'
      },
      minWidth: {
        24: '6rem',
        16: '4rem'
      },
      height: {
        15: '3.75rem',
        30: '7.5rem'
      },
      width: {
        30: '7.5rem'
      },
      lineHeight: {
        15: '3.75rem'
      },
      padding: {
        80: '20rem'
      },
      margin: {
        15: '3.75rem'
      },
      fill: (theme) => ({
        primary: theme('colors.primary.default'),
        white: theme('colors.white'),
        gray: theme('colors.gray')
      })
    },
    colors: {
      green: {
        DEFAULT: GREEN,
        dark: DARK_GREEN
      },
      red: {
        DEFAULT: RED
      },
      blue: {
        DEFAULT: BLUE,
        dark: DARK_BLUE
      },
      cyan: {
        dark: DARK_CYAN
      },
      white: WHITE,
      black: {
        DEFAULT: BLACK,
        dark: DARK_BLACK
      },
      gray: {
        lighter: LIGHTER_GRAY,
        light: LIGHT_GRAY,
        DEFAULT: GRAY,
        dark: DARK_GRAY,
        darker: DARKER_GRAY
      },
      silver: SILVER,
      bronze: BRONZE,
      transparent: 'transparent'
    },
    fontFamily: {
      display:
        'BigNoodleTitling-Oblique, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";',
      display_straight:
        'BigNoodleTitling, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";',
      body:
        'Roboto, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";'
    },
    boxShadow: {
      DEFAULT: '0px 3px 12px rgba(0, 0, 0, 0.25)'
    }
  },
  variants: {},
  plugins: [require('@tailwindcss/typography')]
}
