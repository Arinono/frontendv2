import React from 'react'
import styled from 'styled-components'

function LoginPage() {
  // maybe we will not need this page if we have full twitch login
  return <Container>Login</Container>
}

const Container = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

export default LoginPage
