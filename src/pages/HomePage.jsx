import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import Logo from '../ui/Logo'
import StyledButton from '../ui/Button'
import { useEvents } from '../store/events'
import HomeEvents from '../components/HomeEvents'
import { motion } from 'framer-motion'
import { useMediaQuery } from '@react-hook/media-query'

function HomePage() {
  const { events } = useEvents()
  const isMobile = !useMediaQuery('(min-width: 640px)')

  let highlighted_events = []
  if (events !== null) {
    highlighted_events = events
      .sort((a, b) => {
        if (a.isLive !== a.isLive) {
          return a.isLive ? 1 : -1
        }
        if (a.isFuture !== a.isFuture) {
          return a.isFuture ? 1 : -1
        }
        if (a.isPast === true) {
          return a.startEventAt < b.startEventAt ? 1 : -1
        }
        return a.startEventAt > b.startEventAt ? 1 : -1
      })
      .slice(0, 4)
  }

  return (
    <Container>
      <LogoContainer layoutId="logo">
        <Logo />
      </LogoContainer>
      <Summary>
        <FormattedMessage id="home.summary" />
      </Summary>
      {!!highlighted_events.length && (
        <HomeEvents
          events={highlighted_events}
          size={isMobile ? 'small' : 'medium'}
        />
      )}
      <Link to="/evenements">
        <StyledButton>
          <FormattedMessage id="home.events_link" />
        </StyledButton>
      </Link>
    </Container>
  )
}

const Container = styled.div`
  ${tw`h-full flex justify-center items-center flex-col py-10`};
`

const LogoContainer = styled(motion.div)`
  max-width: calc(100% - 1rem);
  width: 20rem;
`

const Summary = styled.div`
  color: white;
  background-color: #092638;
  line-height: 1.4rem;
  margin-block-start: 1.8rem;
  margin-block-end: 1.8rem;
  max-width: 25rem;
  text-align: justify;
  padding: 1rem;
  position: relative;
  ${tw`mx-4 text-sm sm:text-base`}

  &::before {
    ${tw`absolute border-t-4 border-primary left-0 top-0 w-2/6 whitespace-pre `}
    content: ' ';
  }
`

export default HomePage
