import React, { useEffect } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { IntlProvider } from 'react-intl'
import { NotificationsProvider } from './store/notification'
import './app.css'
import './icons'
import HomePage from './pages/HomePage'
import About from './pages/About'
import BaseLayout from './layouts/BaseLayout'
import { useLanguage } from './store/language'
import EventPage from './pages/EventPage'
import LoginPage from './pages/LoginPage'
import EventsPage from './pages/EventsPage'
import UserPage from './pages/UserPage'
import WidgetsPage from './pages/WidgetsPage'
import AuthManager from './components/AuthManager'
import TwitchAuth from './pages/TwitchAuth'
import ApiAuth from './pages/ApiAuth'
import { useEvents } from './store/events'
import { AnimateSharedLayout } from 'framer-motion'
import ProfilePage from './pages/Profile'
import { EventProvider } from './store/event'
import { LayoutProvider } from './store/layout'
import ConnectedEventPage from './components/ConnectedEventPage'
import ConnectedLiveEvent from './components/ConnectedLiveEvent'

const App = () => {
  const { language, messages } = useLanguage()
  const { loadEvents } = useEvents()
  useEffect(() => {
    loadEvents()
  }, [])

  return (
    <IntlProvider locale={language} messages={messages} defaultLocale="fr">
      <NotificationsProvider>
        <AuthManager>
          <AnimateSharedLayout>
            <Router>
              <Switch>
                <Route path="/auth">
                  <Switch>
                    <Route exact path="/auth/api">
                      <ApiAuth />
                    </Route>
                    <Route exact path="/auth/twitch">
                      <TwitchAuth />
                    </Route>
                  </Switch>
                </Route>
                <Route path="/">
                  <LayoutProvider>
                    <BaseLayout>
                      <Switch>
                        <Route exact path="/connexion">
                          <LoginPage />
                        </Route>
                        <Route path="/direct">
                          <ConnectedLiveEvent />
                        </Route>
                        <Route exact path="/evenements">
                          <EventsPage />
                        </Route>
                        <Route path="/evenement/:eventId">
                          <ConnectedEventPage />
                        </Route>
                        <Route exact path="/participants">
                          <UserPage />
                        </Route>
                        <Route exact path="/participant/:name">
                          <UserPage />
                        </Route>
                        <Route exact path="/widget">
                          <WidgetsPage />
                        </Route>
                        <Route exact path="/about">
                          <About />
                        </Route>
                        <Route exact path="/profil">
                          <ProfilePage />
                        </Route>
                        <Route exact path="/">
                          <HomePage />
                        </Route>
                        <Route path="/">
                          {
                            // on peut mettre une vraie page de 404 sinon
                          }
                          <Redirect to="/" />
                        </Route>
                      </Switch>
                    </BaseLayout>
                  </LayoutProvider>
                </Route>
              </Switch>
            </Router>
          </AnimateSharedLayout>
        </AuthManager>
      </NotificationsProvider>
    </IntlProvider>
  )
}
export default App
