import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

/**
 *
 * @param src string
 * @param alt string
 * @param shape 'circle'|'square'
 * @constructor
 */
const Media = ({ src, alt, shape, className = '', ...restProps }) => {
  return (
    <div {...restProps} className={`${className} shape-${shape}`}>
      <Container>
        <Image src={src} alt={alt} shape={shape} />
      </Container>
    </div>
  )
}

const Image = styled.img`
 ${tw`absolute top-0 left-0 h-full w-full`}
 ${(props) => {
   switch (props.shape) {
     case 'circle':
       return tw`rounded-full`
     case 'square':
       return tw`rounded-none`
   }
 }}}

`

const Container = styled.div`
  padding-top: 100%;
  ${tw`relative`}
`

export default Media
