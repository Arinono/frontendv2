import React, { useCallback, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import debounce from 'lodash.debounce'
import { findAccounts } from '@breci/eventz_api'
import config from '../../config'
import { useUsersInfo } from '../../store/usersInfo'
import useOnClickOutside from '../../hooks/useClickOutside'

const UserSearch = ({ onUserSelected, value }) => {
  const ref = useRef()
  const [users, setUsers] = useState([])
  const [loading, setLoading] = useState(false)
  const [userName, setUsername] = useState('')
  const { loadUsers, getUsers } = useUsersInfo()

  const [showDropDown, setShowDropDown] = useState(false)

  useOnClickOutside(ref, () => setShowDropDown(false))

  const updateUserList = useCallback(
    debounce((username) => {
      setLoading(true)
      findAccounts(username, config.API_ORGANISATION_ID, {
        page: 0,
        size: 5
      }).then(async (res) => {
        if (res.data.content) {
          const ids = res.data.content.map((u) => u.passportId)
          const fullUsers = await loadUsers(ids, true)
          setUsers(fullUsers)

          setLoading(false)
        }
      })
    }, 250),
    []
  )
  const onInput = useCallback((e) => {
    const username = e.target.value

    if (username) {
      updateUserList(username)
    }
  }, [])

  return (
    <Container ref={ref}>
      <Input
        value={userName}
        type="text"
        onInput={(e) => {
          setShowDropDown(true)
          setUsername(e.target.value)
          onInput(e)
        }}
        placeholder="Nom du joueur..."
      />
      {showDropDown && (
        <DropDown>
          {users.map((user) => (
            <Option
              onClick={() => {
                onUserSelected(user)
                setUsername(user.identity.username)
                setShowDropDown(false)
              }}
              key={user.id}
            >
              {user.identity.username}
            </Option>
          ))}
        </DropDown>
      )}
    </Container>
  )
}

const Container = styled.div`
  ${tw`relative inline-block`}
`

const DropDown = styled.div`
  ${tw`absolute flex flex-col shadow`}
  top:100%;
  width: 100%;
  left: 0;
  z-index: 100;
`

const Option = styled.button`
  ${tw`p-2 text-sm w-full`}
  cursor:pointer;
  &:focus,
  &:active {
    border: none;
    outline: none;
  }
  &:nth-child(even) {
    ${tw`bg-primary`}
  }

  &:nth-child(odd) {
    ${tw`bg-primary-dark`}
  }
`

const Input = styled.input`
  ${tw`rounded bg-black-dark text-white`}
`

export default UserSearch
