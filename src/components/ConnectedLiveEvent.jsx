import React from 'react'
import { EventProvider } from '../store/event'
import EventPage from '../pages/EventPage'
import { useEvents } from '../store/events'

const ConnectedLiveEvent = () => {
  const { nextEvent } = useEvents()

  if (!nextEvent) return null
  return (
    <EventProvider initialState={{ eventId: nextEvent.id }}>
      <EventPage isLive />
    </EventProvider>
  )
}

export default ConnectedLiveEvent
