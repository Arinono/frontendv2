import React from 'react'
import { FormattedMessage } from 'react-intl'
import { format } from 'date-fns'
import { fr } from 'date-fns/locale'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import tw from 'twin.macro'
import LiveTag from '../ui/LiveTag'
import RegistrationTag from '../ui/RegistrationTag'
// replace with API managed content, or better implementation
import header_5f312c0a8bc31e75cb2140a4 from '../assets/events/5f312c0a8bc31e75cb2140a4/list_header.jpg'
import header_5f312d044975c8415c1131e8 from '../assets/events/5f312d044975c8415c1131e8/list_header.jpg'
import header_5f313f3a960cd65b23ce944b from '../assets/events/5f313f3a960cd65b23ce944b/list_header.jpg'
import header_5f3140471cc7be197799f313 from '../assets/events/5f3140471cc7be197799f313/list_header.jpg'

const DateFormat = 'PPPPp'
const headers = {
  '5f312c0a8bc31e75cb2140a4': header_5f312c0a8bc31e75cb2140a4,
  '5f312d044975c8415c1131e8': header_5f312d044975c8415c1131e8,
  '5f313f3a960cd65b23ce944b': header_5f313f3a960cd65b23ce944b,
  '5f3140471cc7be197799f313': header_5f3140471cc7be197799f313
}

const Events = ({ events }) => {
  return (
    <div>
      {events.map((event) => {
        let date_label = format(event.startEventAtDate, DateFormat, {
          locale: fr
        })
        if (!event.lastOneDay) {
          date_label = (
            <FormattedMessage
              id="events.date_range"
              values={{
                start: date_label,
                end: format(event.endEventAtDate, DateFormat, { locale: fr })
              }}
            />
          )
        }

        let header = ''
        if (headers[event.id] !== undefined) {
          header = headers[event.id]
        }

        return (
          <Event
            key={event.id}
            to={'/evenement/' + event.id}
            background={header}
          >
            {event.isLive && <LiveTag />}
            {event.isRegistrationOpen && <RegistrationTag />}
            <Name>{event.name}</Name>
            <DateDisplay>{date_label}</DateDisplay>
          </Event>
        )
      })}
    </div>
  )
}

const Event = styled(Link)`
  ${tw`block p-4 mt-4 rounded relative`}
  background: linear-gradient(50deg, rgba(67, 168, 29,0.4) 0%, rgba(20, 23, 21, 1) 16%, rgba(20, 23, 21, 1) 60%, rgba(20,23,21,0.4) 80%),
   center right / auto 100% no-repeat url(${(props) => props.background});
`
const Name = styled.div``
const DateDisplay = styled.div`
  ${tw`opacity-50 text-sm`}
`

export default Events
