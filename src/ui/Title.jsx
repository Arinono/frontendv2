import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

const Title = styled.div`
  ${tw`relative leading-none uppercase`}
  ${(props) => {
    switch (props.level) {
      case 1:
      default:
        return tw` pt-8 pb-3 mb-8 text-3xl sm:text-5xl min-w-24 font-display_straight`
      case 2:
        return tw` pb-4 text-2xl sm:text-3xl min-w-24 font-display`
      case 3:
        return tw` pb-3 text-xl sm:text-2xl min-w-16 font-display`
    }
  }}
  ${(props) => (props.theme === 'dark' ? tw`text-white` : tw`text-black`)}
  ${(props) => (props.center ? tw`text-center` : tw`text-left`)}

  &:after {
    content: ' ';
    ${tw`absolute bottom-0 h-1 bg-primary`}
    ${(props) => {
      switch (props.level) {
        case 1:
          return tw`hidden`
        case 2:
          return tw`bg-primary w-24`
        case 3:
        default:
          return tw`bg-secondary w-16`
      }
    }}
    ${(props) =>
      props.center ? tw`left-1/2 transform -translate-x-1/2` : tw`left-0`}
  }
`

Title.defaultProps = {
  level: 1,
  center: false,
  theme: 'dark'
}

export default Title
