export default {
  title: 'FFS',
  credits: {
    craft: 'Fabriqué avec {love}',
    contact: 'Nous contacter',
    legal: 'CGU'
  },
  home: {
    summary:
      "Une centaine de streamers s'affrontent lors d'une compétition sur des jeux, organisée et commentée par ZeratoR. Tous les participants de la compétition doivent s'abonner à la chaîne Twitch™ du vainqueur.",
    events_link: 'Toutes les éditions',
    join_button: 'participer',
    learn_more: 'en savoir plus',
    watch_now: 'Regardez maintenant'
  },
  menu: {
    live_link: 'événement en cours',
    open_link: 'événement à venir',
    events_link: 'événements',
    about_link: 'a propos',
    information_link: 'informations',
    participants_link: 'participants',
    ranking_link: 'classement',
    login_link: 'Se connecter',
    logout_link: 'Déconnexion'
  },
  event: {
    summary_title: 'présentation',
    live_title: 'Direct',
    information_title: 'informations',
    join_button: 'participer',
    date: "Date de l'évènement",
    event_unregister_button: 'Se désinscrire',
    event_joined_button: 'Déjà Inscrit !',
    event_open_team_management: "Gérer l'équipe",
    event_logged_off_message: "Connecte toi pour t'inscrire à l'évènement",
    registration_not_started: 'Inscriptions à venir!',
    registration_ended: 'Inscriptions terminées!',
    registration_start_date: "Date de début de l'inscription",
    registration_end_date: "Date de fin de l'inscription",
    event_start_date: "Date de début de l'événement",
    event_end_date: "Date de fin de l'événement",
    registration_rules_title: "Conditions d'inscription",
    participants_title: 'participants ({count})',
    registered_title: 'inscrits ({count})',
    registration_rules: {
      TWITCH_views_BIGGER: {
        label: 'Nombre de vues minimum',
        value: '{value} vues'
      },
      TWITCH_followers_BIGGER: {
        label: 'Nombre de followers minimum',
        value: '{value} followers'
      }
    },
    unregister: {
      message: 'êtes-vous sûr de vouloir vous désinscrire?',
      cancel: 'Annuler',
      ok: 'Se Désinscrire'
    },
    not_eligible: {
      message: "Vous n'avez pas les prérequis pour participer à l'événement",
      rule_header: 'Régle',
      prerequisite_header: 'Attendu',
      user_header: 'Vous',
      notice:
        'Si les informations semblent erronées, vous pouvez les actualiser:',
      reconnect_link: 'Actualiser'
    },
    ranking: {
      title: 'Classement',
      global: 'Général',
      position: '#',
      player: 'Joueur',
      score: 'Score'
    }
  },
  events: {
    title: 'événements',
    pastEventTitle: 'événements passés',
    no_events: 'aucun événément',
    date_range: 'Du {start} au {end}',
    registration_tag: 'Inscriptions ouvertes',
    live: 'en direct'
  },
  dates: {
    format: 'PPPP',
    range_label: 'Du {start} au {end}',
    same_day_label: '{date} de {start_hour} à {end_hour}'
  }
}
