import * as Sentry from '@sentry/react'

export const initSentry = (dsn, environment) => Sentry.init({ dsn, environment })
