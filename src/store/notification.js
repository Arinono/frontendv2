import { useCallback, useState } from 'react'
import { createContainer } from 'unstated-next'

/*
interface Notification {
    id: string;
    type: "success" | "error" | "warning";
    message : string;
    duration? : number
}
 */

function useNotificationHook() {
  const [notifications, setNotifications] = useState([])

  const addNotification = useCallback(({ message, type, duration = 0 }) => {
    const id = Date.now().toString(36)
    const newNotification = {
      id,
      message,
      type,
      duration
    }
    setNotifications((n) => [...n, newNotification])
    if (duration) {
      setTimeout(() => {
        removeNotification(id)
      }, duration)
    }
  }, [])

  const removeNotification = useCallback((id) => {
    setNotifications((n) => {
      const i = n.findIndex((notif) => notif.id === id)
      if (i !== -1) {
        const newArray = [...n]
        newArray.splice(i, 1)

        return newArray
      }
      return n
    })
  }, [])

  return { notifications, addNotification, removeNotification }
}

let Notifications = createContainer(useNotificationHook)

export const NotificationsProvider = Notifications.Provider
export const useNotifications = Notifications.useContainer
