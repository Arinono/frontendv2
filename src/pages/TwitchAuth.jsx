import { useEffect } from 'react'
import { useUser } from '../store/user'
import { updateIdentity, updateMeAccount } from '@breci/eventz_api'

const AUTH_KEY = 'access_token'

const TwitchAuth = () => {
  const { setTwitchUserToken, twitchToken, token, tokenInitialized } = useUser()
  useEffect(() => {
    if (window.location.hash) {
      const pairs = window.location.hash
        .substring(1)
        .split('&')
        .map((value) => value.split('='))

      const object = pairs.reduce(
        (reducer, [key, value]) => ({
          ...reducer,
          [key]: value
        }),
        {}
      )

      setTwitchUserToken(object[AUTH_KEY])
      window.location.hash = ''

      // redirect will be handled on the AuthManager
    }
  }, [])

  useEffect(() => {
    if (twitchToken && token && tokenInitialized) {
      updateIdentity('TWITCH', `Bearer ${twitchToken}`).then((res) => {
        const data = res.data
        if (data && data.identity) {
          updateMeAccount({
            username: data.identity.username
          }).then(() => {
            window.location.replace('/')
          })
        }
      })
    }
  }, [twitchToken, token, tokenInitialized])
  return null
}

export default TwitchAuth
