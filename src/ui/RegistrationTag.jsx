import { FormattedMessage } from 'react-intl'
import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

const RegistrationTag = ({ ...restProps }) => (
  <Tag {...restProps}>
    <FormattedMessage id="events.registration_tag" />
  </Tag>
)

const Tag = styled.div`
  ${tw`absolute right-0 top-0 bg-green px-1 capitalize rounded-sm m-2`}
`

export default RegistrationTag
