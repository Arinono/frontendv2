import React, { useEffect, useMemo } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import { FormattedMessage } from 'react-intl'
import Card from '../ui/Card'
import ContentPage from '../ui/ContentPage'
import ErrorMessage from '../ui/ErrorMessage'
import Title from '../ui/Title'
import Events from '../components/Events'
import { useEvents } from '../store/events'

const EventsPage = () => {
  const { events, loading } = useEvents()

  const sortedNextEvents = useMemo(
    () =>
      events
        .filter((event) => event.isLive || event.isFuture)
        .sort((a, b) => a.startEventAt - b.startEventAt),
    [events]
  )

  const sortedPastEvents = useMemo(
    () =>
      events
        .filter((event) => !(event.isLive || event.isFuture))
        .sort((a, b) => b.startEventAt - a.startEventAt),
    [events]
  )

  if (loading) {
    return null
  }

  return (
    <ContentPage>
      <Card className="w-full">
        {events.length <= 0 && (
          <ErrorMessage title={'events.no_events'} message={'Ya rien'} />
        )}
        {!!events.length && (
          <div>
            {!!sortedNextEvents.length && (
              <EventsBlock>
                <Title level={2}>
                  <FormattedMessage id="events.title" />
                </Title>
                <Events events={sortedNextEvents} />
              </EventsBlock>
            )}
            {!!sortedPastEvents.length && (
              <EventsBlock>
                <Title level={3}>
                  <FormattedMessage id="events.pastEventTitle" />
                </Title>
                <Events events={sortedPastEvents} />
              </EventsBlock>
            )}
          </div>
        )}
      </Card>
    </ContentPage>
  )
}

const EventsBlock = styled.div`
  ${tw`mb-4`}
`

export default EventsPage
