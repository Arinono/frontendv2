import React from 'react'

const CrownIcon = (restProps) => {
  return (
    <svg viewBox="0 0 42 23" xmlns="http://www.w3.org/2000/svg" {...restProps}>
      <path d="M9.92405 10.876L21.0886 0L32.7848 11.2326L42 4.10078L40.5823 23H1.41772L0 3.74419L9.92405 10.876Z" />
    </svg>
  )
}

export default CrownIcon
