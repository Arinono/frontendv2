/* eslint no-undef: 0 */

export function initGA() {
  window.dataLayer = window.dataLayer || []
  function gtag() {
    dataLayer.push(arguments)
  }
  gtag('js', new Date())
  gtag('config', 'UA-175049010-1')
}
