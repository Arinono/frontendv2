import { useCallback, useState } from 'react'
import { createContainer } from 'unstated-next'
import {
  getMeTeams,
  getInvites,
  getMeTeamForEvent,
  getTeams
} from '@breci/eventz_api'

import config from '../config'

function useTeamsHook() {
  const [loading, setLoading] = useState(false)
  const [teams, setTeams] = useState([])
  const [pendingInvites, setPendingInvites] = useState([])
  const [acceptedInvites, setAcceptedInvites] = useState([])

  const loadMeTeamForEvent = useCallback(async (eventId) => {
    setLoading(true)
    const response = await getMeTeamForEvent(eventId)

    setTeams((teams) => [
      ...teams.filter((t) => t.id !== response.data.data.id),
      ...response.data.data
    ])
    setLoading(false)
    return response.data.data
  }, [])

  const loadMeTeams = useCallback(async () => {
    setLoading(true)
    const response = await getMeTeams()

    // TODO Test it a bit more
    setTeams((teams) => [
      ...teams.filter(
        (t) => !response.data.content.find((_t) => _t.id === t.id)
      ),
      response.data.content
    ])
    setLoading(false)
    return response.data.content
  }, [])

  const loadTeamsFromEvent = useCallback(async (eventId) => {
    const eventTeams = (await getTeams({ size: 1000, page: 0 }, eventId)).data
      .content
    setTeams((teams) => [
      ...teams.filter((t) => !eventTeams.find((_t) => _t.id === t.id)),
      ...eventTeams
    ])
  }, [])

  const loadPendingInvites = useCallback(async (eventId) => {
    setLoading(true)
    const response = await getInvites(
      {
        size: 1000,
        page: 0
      },
      'AWAITING'
    )

    setPendingInvites(response.data.content)
    setLoading(false)
  }, [])

  const loadAcceptedInvites = useCallback(async (eventId) => {
    setLoading(true)
    const response = await getInvites(
      {
        size: 1000,
        page: 0
      },
      'ACCEPTED'
    )

    setAcceptedInvites(response.data.content)
    setLoading(false)
  }, [])

  return {
    loading,
    teams,
    pendingInvites,
    acceptedInvites,
    loadMeTeams,
    loadPendingInvites,
    loadAcceptedInvites,
    loadMeTeamForEvent
  }
}

let Teams = createContainer(useTeamsHook)

export const TeamsProvider = Teams.Provider
export const useTeams = Teams.useContainer
