import tw from 'twin.macro'
import styled from 'styled-components'

const Button = styled.button`
  ${tw`outline-none rounded transition-all duration-100 block uppercase m-auto`}
  ${(props) =>
    props.hollow
      ? tw`border-2 border-primary bg-transparent text-primary`
      : tw`border-2 border-primary bg-green text-white`}
  ${(props) => props.size === 'lg' && tw`text-base py-3 px-10 font-semibold`}
  ${(props) => props.size === 'sm' && tw`text-sm py-1 px-6`}
  ${(props) => props.size === 'xs' && tw`text-xs py-1 px-6`}
  &:focus {
    ${tw`outline-none`}
  }
  &:active {
    transform: translateY(0.1rem);
  }

  &:disabled {
    ${tw`cursor-default opacity-50`}
  }
`

Button.defaultProps = {
  hollow: false,
  size: 'lg'
}

export default Button
