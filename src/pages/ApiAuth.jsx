import React, { useEffect } from 'react'
import { useUser } from '../store/user'

const AUTH_KEY = 'access_token'

const ApiAuth = () => {
  const { setUserToken } = useUser()
  useEffect(() => {
    if (window.location.hash) {
      const pairs = window.location.hash
        .substring(1)
        .split('&')
        .map((value) => value.split('='))

      const object = pairs.reduce(
        (reducer, [key, value]) => ({
          ...reducer,
          [key]: value
        }),
        {}
      )

      if (object[AUTH_KEY]) {
        setUserToken(object[AUTH_KEY])
        window.location.hash = ''
      }
    }
  }, [])
  return null
}

export default ApiAuth
