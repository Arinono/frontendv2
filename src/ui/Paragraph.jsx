import React from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'

const Paragraph = ({ children, ...props }) => {
  return (
    <Container>
      {props.label && (
        <Label>
          <FormattedMessage id={props.label} />
        </Label>
      )}
      {children}
    </Container>
  )
}

const Label = styled.div`
  ${tw`block text-gray text-sm leading-none`}
`

const Container = styled.div`
  ${tw`block mt-1 mb-1 text-base`}
`

export default Paragraph
