import Card from '../ui/Card'
import Button from '../ui/Button'
import React, { useCallback, useMemo } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

const InvitationCard = ({ image, sender, onAccept, onDecline }) => {
  const text = useMemo(() => {
    return `${sender} vous a invité dans son équipe`
  }, [sender])

  const onAcceptClick = useCallback(() => {}, [])

  const onDeclineClick = useCallback(() => {}, [])
  return (
    <CardContainer>
      <div>
        <Media src={image} />
      </div>
      <Text>{text}</Text>
      <Actions>
        <Button size="xs" onClick={onAcceptClick}>
          Accepter
        </Button>
        <Button size="xs" hollow onClick={onDeclineClick}>
          Décliner
        </Button>
      </Actions>
    </CardContainer>
  )
}

const CardContainer = styled(Card)`
  ${tw`flex flex-row`}
`

const Text = styled.div`
  ${tw`text-sm flex-1`}
`

const Actions = styled.div`
  ${tw`flex flex-row`}
  & > * {
    ${tw`mx-2`}
  }
`

const Media = styled.img``

export default InvitationCard
