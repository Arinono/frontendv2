import { useCallback, useEffect, useMemo, useState } from 'react'
import { createContainer } from 'unstated-next'
import { useUsersInfo } from './usersInfo'
import { getRanking } from '@breci/eventz_api'

const ANONYMOUS_IDENTITY = {
  // default identity to have a placeholder
  profileImageURL: '',
  username: 'Anonyme',
  login: 'zerator',
  views: 0,
  followers: 0
}

function useRankHook(
  initialState = {
    roundId: ''
  }
) {
  const { loadUsers, getUser } = useUsersInfo()

  const [ranking, setRanking] = useState([])
  const [rankingLoading, setRankingLoading] = useState(false)
  const [rankingInitialized, setRankingInitialized] = useState(false)

  const [usersInfoLoading, setUsersInfoLoading] = useState(false)
  const [usersInfoInitialized, setUsersInfoInitialized] = useState(false)

  const loadRanking = useCallback(async () => {
    setRankingLoading(true)
    const r = await getRanking(initialState.roundId, {
      size: 1000,
      page: 0
    }).then((resp) => resp.data.content)

    setRanking(r)
    setRankingInitialized(true)
    setRankingLoading(false)
  }, [initialState.roundId])

  useEffect(() => {
    async function f() {
      await loadRanking()
      await loadUserDataFromRounds
    }
    f()
  }, [initialState.roundId])

  const extendedRankingWithMembers = useMemo(() => {
    return ranking.map((r) => {
      return {
        ...r,
        team: {
          ...r.team,
          members: r.team.members.map(
            (m) =>
              getUser(m) || {
                id: m.id,
                identity: { ...ANONYMOUS_IDENTITY }
              }
          )
        }
      }
    })
  }, [rankingInitialized, ranking])

  const loadUserDataFromRounds = useCallback(async () => {
    setUsersInfoLoading(true)
    const users = await loadUsers(ranking.teams.members.flat())
    setUsersInfoInitialized(true)
    setUsersInfoLoading(false)
  }, [])

  return {
    ranking,
    extendedRankingWithMembers,
    loadRanking,
    rankingInitialized,
    rankingLoading,
    usersInfoLoading,
    usersInfoInitialized
  }
}

let Rank = createContainer(useRankHook)

export const RankProvider = Rank.Provider
export const useRank = Rank.useContainer
