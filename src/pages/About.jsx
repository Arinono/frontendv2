import React from 'react'
import styled from 'styled-components'

function About() {
  return <Container>About us</Container>
}

const Container = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

export default About
