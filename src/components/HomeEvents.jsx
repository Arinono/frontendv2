import React from 'react'
import { format } from 'date-fns'
import { fr } from 'date-fns/locale'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import tw from 'twin.macro'
import LiveTag from '../ui/LiveTag'
import Title from '../ui/Title'
import Button from '../ui/Button'
import { FormattedMessage } from 'react-intl'
import PlayIcon from '../ui/Icons/PlayIcon'
import LiveThumbnail from '../assets/live_thumbnail.jpg'

const HomeEvents = ({ events, size = 'medium' }) => (
  <Container>
    {events.map((event) => {
      let date_label_key = event.lastOneDay
        ? 'dates.same_day_label'
        : 'dates.range_label'
      let date_label_values = {}

      if (event.lastOneDay) {
        date_label_values = {
          date: format(
            event.startEventAtDate,
            size === 'medium' ? 'PPPP' : 'P',
            { locale: fr }
          ),
          start_hour: format(event.startEventAtDate, 'p', { locale: fr }),
          end_hour: format(event.startEventAtDate, 'p', { locale: fr })
        }
      } else {
        date_label_values = {
          start: format(
            event.startEventAtDate,
            size === 'medium' ? 'PPPPp' : 'Pp',
            { locale: fr }
          ),
          end: format(
            event.endEventAtDate,
            size === 'medium' ? 'PPPPp' : 'Pp',
            { locale: fr }
          )
        }
      }

      return (
        <Event key={event.id} to={'/evenement/' + event.id} live={event.isLive}>
          {event.isLive && <LiveTag style={tw`z-10`} />}
          <StyledTitle level={3} style={tw`z-10`}>
            {event.name}
          </StyledTitle>
          <DateDisplay>
            <FormattedMessage id={date_label_key} values={date_label_values} />
          </DateDisplay>
          <Button size="xs" style={tw`z-10`}>
            <FormattedMessage
              id={event.isLive ? 'home.watch_now' : 'home.learn_more'}
            />
          </Button>
          {event.isLive && (
            <div>
              <PlayIconContainer>
                <StyledPlayIcon>
                  <PlayIcon />
                </StyledPlayIcon>
              </PlayIconContainer>
              <BackgroundContainer>
                <Background />
              </BackgroundContainer>
            </div>
          )}
        </Event>
      )
    })}
  </Container>
)

const Container = styled.div`
  ${tw`m-6 max-w-2xl flex flex-wrap items-stretch content-center justify-around`}
`

const Event = styled(Link)`
  ${tw`relative block p-3 mb-3 h-48
   self-stretch flex flex-col flex-nowrap items-stretch
   rounded shadow bg-opacity-50 text-white`}
  ${(props) => !props.live && tw`bg-black`}
  ${(props) => props.live && tw`bg-primary`}
  width: 48%;
  min-width: 15rem;
`

const StyledTitle = styled(Title)`
  ${tw`flex-none truncate`}
`

const DateDisplay = styled.div`
  ${tw`opacity-50 text-sm m-3 flex-grow text-center`}
`

const PlayIconContainer = styled.div`
  ${tw`absolute inset-0 z-10 p-4 flex items-center justify-center`}
`

const StyledPlayIcon = styled.div`
  ${tw`block w-12 h-12 self-center text-white`}
`

const Background = styled.div`
  ${tw`absolute z-0 bg-center bg-cover`}
  background-repeat: no-repeat;
  background-image: url(${LiveThumbnail});

  top: -5px;
  left: -5px;
  bottom: -5px;
  right: -5px;
  -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);
`

const BackgroundContainer = styled.div`
  ${tw`absolute inset-0 z-0 rounded overflow-hidden m-1`}
`

export default HomeEvents
