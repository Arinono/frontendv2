import React from 'react'
import { EventProvider } from '../store/event'
import EventPage from '../pages/EventPage'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from 'react-router-dom'
const ConnectedEventPage = () => {
  let { eventId } = useParams()

  return (
    <EventProvider initialState={{ eventId }}>
      <EventPage />
    </EventProvider>
  )
}

export default ConnectedEventPage
