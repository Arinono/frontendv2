/*
There is a problem with font awesome packages, so I remove the warnings spamming the console
 */

module.exports = function plugin(options) {
  return {
    name: 'cleanWarn',
    options: (options) => {
      const prevWarn = options.onwarn
      options.onwarn = (warning, warn) => {
        if (warning.code === 'THIS_IS_UNDEFINED') return
        if (prevWarn) prevWarn(warning, warn)
      }
    }
  }
}
