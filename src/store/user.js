import { useCallback, useEffect, useMemo, useState } from 'react'
import { createContainer } from 'unstated-next'
import { getIdentity, getMeTeams, setAuthToken } from '@breci/eventz_api'
import config from '../config'

function useUsersHook(initialState = { user: null, teams: [] }) {
  // Data
  const [token, setToken] = useState(localStorage.getItem('token') || '')
  const [twitchToken, setTwitchToken] = useState(
    localStorage.getItem('twitch-token') || ''
  )
  const [user, setUser] = useState(initialState.user)
  const [teams, setTeams] = useState(initialState.teams)

  const [error, setError] = useState(null)

  const [tokenInitialized, setTokenInitialized] = useState(false)

  // Loading info
  const [loadingUser, setLoadingUser] = useState(false)
  const [loadingTeams, setLoadingTeams] = useState(false)
  const loading = useMemo(
    () => loadingUser || loadingTeams,
    [loadingTeams, loadingUser]
  )

  const setUserToken = useCallback(
    (token) => {
      localStorage.setItem('token', token)
      setToken(token)
      setTokenInitialized(true)
    },
    [setToken, setTokenInitialized]
  )

  const setTwitchUserToken = useCallback(
    (token) => {
      localStorage.setItem('twitch-token', token)
      setTwitchToken(token)
    },
    [setTwitchToken]
  )

  // Load data
  const loadUserInfo = useCallback(async () => {
    setLoadingUser(true)
    getIdentity('TWITCH')
      .then((resp) => {
        setUser(resp.data)
        setLoadingUser(false)
      })
      .catch((err) => {
        if (err.response?.status === 404) {
          // TODO handle race condition here
          window.location.replace(
            `https://id.twitch.tv/oauth2/authorize?client_id=${config.API_CLIENT_ID_FIX}&redirect_uri=${window.location.origin}/auth/twitch?
            response_type=token&scope=user:read:email`
          )
        }
        if (err.response?.status === 401) {
          // refresh token
          localStorage.setItem('AUTH_PREVIOUS_URL', window.location.href)
          window.location.replace(
            `${config.AUTH_URL}/login?organisation=${config.API_ORGANISATION_ID}&response_type=token&client_id=${config.API_CLIENT_ID}&redirect_uri=${window.location.origin}/auth/api&scope=`
          )
        }
      })
  }, [setUser])

  const loadUserTeams = useCallback(async () => {
    if (user) {
      setLoadingTeams(true)
      const resp = await getMeTeams()
      setTeams((t) => [
        ...t.filter(
          (_t) => !resp.data.content.find((respTeam) => respTeam.id === _t.id)
        ),
        ...resp.data.content
      ])
      setLoadingTeams(false)
    }
  }, [user])

  // helpers
  const isLoggedIn = useMemo(() => user && !!user.token, [user])

  // Actions
  const logIn = useCallback(() => {
    localStorage.setItem(
      'AUTH_PREVIOUS_URL',
      window.location.href
    )
  }, [])

  const logOff = useCallback(
    () => {
      // clear the local storage
      localStorage.removeItem('token')
      localStorage.removeItem('twitch-token')
      // clear the user store
      setTwitchToken()
      setToken()
      setAuthToken()
      setTokenInitialized(false)
      setUser(initialState.user)
      setTeams(initialState.teams)
      // Redirect to root page
      window.location = '/'
    },
    [
      setTwitchToken,
      setToken,
      setTokenInitialized,
      setUser,
      setTeams,
      initialState.user,
      initialState.teams
    ]
  )

  const addTeam = useCallback((team) => {
    setTeams((t) => [...t, team])
  }, [setTeams])
  const removeTeam = useCallback((id) => {
    setTeams((t) => [...t.filter((team) => team.id !== id)])
  }, [setTeams])

  useEffect(() => {
    if (user) {
      loadUserTeams()
    }
  }, [user, loadUserTeams])

  useEffect(() => {
    if (token) {
      setAuthToken(token)
      setTokenInitialized(true)
    }
  }, [token])

  return {
    token,
    twitchToken,
    user,
    teams,

    tokenInitialized,

    setUserToken,
    setTwitchUserToken,

    loading,
    loadingUser,
    loadingTeams,

    loadUserInfo,
    loadUserTeams,

    addTeam,
    removeTeam,

    isLoggedIn,

    logIn,
    logOff
  }
}

let User = createContainer(useUsersHook)

export const UserProvider = User.Provider
export const useUser = User.useContainer
