import React, { useMemo } from 'react'
import PeopleIcon from './PeopleIcon'
import EyeIcon from './EyeIcon'
import CrownIcon from './CrownIcon'

const Icon = ({ icon, ...restProps }) => {
  const ElementIcon = useMemo(() => {
    switch (icon) {
      case 'people':
        return PeopleIcon
      case 'eye':
        return EyeIcon
      case 'crown':
        return CrownIcon
    }
  }, [icon])

  if (!ElementIcon) {
    return null
  }
  return <ElementIcon {...restProps} />
}

export default Icon
