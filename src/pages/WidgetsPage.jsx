import React from 'react'
import styled from 'styled-components'

function WidgetsPage() {
  return <Container>widgets</Container>
}

const Container = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

export default WidgetsPage
