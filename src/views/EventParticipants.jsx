import React, { useMemo } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'
import { FormattedMessage } from 'react-intl'
import Teams from '../components/Teams'
import Card from '../ui/Card'
import ErrorMessage from '../ui/ErrorMessage'
import Title from '../ui/Title'
import { useMediaQuery } from '@react-hook/media-query'

const EventParticipants = ({ event, teams, users, user }) => {
  const isMobile = !useMediaQuery('(min-width: 640px)')

  //FIXME Replace with store call
  let invites = [
    {
      teamId: 'someTeamId',
      status: 'ACCEPTED'
    },
    {
      teamId: 'someOtherTeamId',
      status: 'AWAITING'
    }
  ]

  const accepted_teams = useMemo(() => {
    if (teams && event) {
      return teams
        .filter((invite) => invite.status === 'VALIDATED')
        .map((team) => {
          return {
            ...team,
            identities: team.memberIds.map((m) =>
              users.find((user) => user.accountId === m)
            ) // populate the identities of the team to easily display them if needed
          }
        })
    }
  }, [teams, users])
  const other_teams = useMemo(() => {
    if (teams && event) {
      return teams
        .filter((invite) => invite.status !== 'VALIDATED')
        .map((team) => {
          return {
            ...team,
            identities: team.memberIds.map((m) =>
              users.find((user) => user.accountId === m)
            ) // populate the identities of the team to easily display them if needed
          }
        })
    }
  }, [teams, users])

  // In case the event is not loaded
  if (!event) {
    return <ErrorMessage title={'events.no_events'} message={'Ya rien'} />
  }

  return (
    <div>
      <Title center={isMobile}>{event.name}</Title>
      <Container>
        <Title level={2}>
          <FormattedMessage
            id="event.participants_title"
            values={{
              count: accepted_teams.length
            }}
          />
        </Title>
        <Teams
          teams={accepted_teams}
          isSolo={!event.groupSize || event.groupSize === 1}
        />
        <Title level={3}>
          <FormattedMessage
            id="event.registered_title"
            values={{
              count: other_teams.length
            }}
          />
        </Title>
        <Teams
          teams={other_teams}
          isSolo={!event.groupSize || event.groupSize === 1}
        />
      </Container>
    </div>
  )
}

const Container = styled(Card)`
  ${tw`w-full`}
`

export default EventParticipants
