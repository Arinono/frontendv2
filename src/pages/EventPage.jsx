import React, { useEffect, useMemo, useState } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import EventInfo from '../views/EventInfo'
import EventParticipants from '../views/EventParticipants'
import EventRanking from '../views/EventRanking'
import ContentPage from '../ui/ContentPage'
import { useEvents } from '../store/events'
import { useUsersInfo } from '../store/usersInfo'
import { useUser } from '../store/user'
import { useNotifications } from '../store/notification'
import { useEvent } from '../store/event'
import { useLayout } from '../store/layout'
import {
  faHome,
  faTrophy,
  faUserFriends
} from '@fortawesome/free-solid-svg-icons'
import { FormattedMessage } from 'react-intl'

const EventPage = ({ isLive = false }) => {
  const { event, eventInitialized, meta } = useEvent()
  let match = useRouteMatch()
  //let match2 = useRouteMatch(match.path)

  const { updateSubmenu } = useLayout()

  const isEventStarted = useMemo(
    () => meta && meta.startEventAt && meta.startEventAt * 1000 < Date.now(),
    [event]
  )

  useEffect(() => {
    if (event) {
      updateSubmenu(
        isEventStarted
          ? [
              {
                to: isLive
                  ? '/direct/informations'
                  : `/evenement/${event.id}/informations`,
                icon: faHome,
                labelKey: 'menu.information_link'
              },
              {
                to: isLive
                  ? '/direct/participants'
                  : `/evenement/${event.id}/participants`,
                icon: faUserFriends,
                labelKey: 'menu.participants_link' // TODO avoir un label pour les équipes et les participants
              }
            ]
          : [
              {
                to: isLive
                  ? '/direct/informations'
                  : `/evenement/${event.id}/informations`,
                icon: faHome,
                labelKey: 'menu.information_link'
              },
              {
                to: isLive
                  ? '/direct/participants'
                  : `/evenement/${event.id}/participants`,
                icon: faUserFriends,
                labelKey: 'menu.participants_link'
              },
              {
                to: isLive
                  ? '/direct/classement'
                  : `/evenement/${event.id}/classement`,
                icon: faTrophy,
                labelKey: 'menu.ranking_link'
              }
            ]
      )
    }
    if (!event) {
      updateSubmenu([])
    }
  }, [event, isEventStarted])

  useEffect(() => {
    return () => {
      updateSubmenu([])
    }
  }, [])

  const { user, teams, addTeam, removeTeam } = useUser()

  const { addNotification } = useNotifications()
  const {
    loadEvent,
    selectedEvent: selectedEventData,
    nextEvent,
    joinEvent,
    leaveEvent,
    addTeamToSelectedEvent,
    deleteTeamToSelectedEvent,
    refreshSelectedEventRounds,
    setSelectedEvent
  } = useEvents()
  const { loadUsers, getUsers } = useUsersInfo()

  const {
    event: selectedEvent,
    rounds: selectedEventRounds
  } = selectedEventData

  useEffect(() => {
    const interval = setInterval(() => {
      //refreshSelectedEventRounds()
    }, 30000)
    return () => clearInterval(interval)
  }, [refreshSelectedEventRounds])

  const [eventLoaded, setEventLoaded] = useState(false)
  const [usersInfoLoaded, setUsersInfoLoaded] = useState(false)

  /*
  LOAD EVENT
   */
  useEffect(() => {
    if (isLive) {
      // loadEvents()
      // setEventLoaded(true)
    } else {
      loadEvent(match.params.eventId).then((e) => {
        setEventLoaded(true)
      })
    }
  }, [match.params.eventId, isLive])

  useEffect(() => {
    if (isLive) {
      if (nextEvent) {
        loadEvent(nextEvent.id)
        setEventLoaded(true)
      } else {
        // TODO determine what to do when there is no direct event
      }
    }
  }, [nextEvent, isLive])

  /*
  LOAD USERS INFOS
   */

  useEffect(() => {
    if (eventLoaded && Object.values(selectedEventData.teams).length) {
      loadUsers(selectedEventData.teams.map((team) => team.memberIds[0])).then(
        () => {
          setUsersInfoLoaded(true)
        }
      )
    } else if (eventLoaded) {
      setUsersInfoLoaded(true)
    }
  }, [eventLoaded, selectedEventData])

  /*
 BUILD DATA
   */

  const eventUsersInfo = useMemo(() => {
    if (usersInfoLoaded) {
      return getUsers(selectedEventData.teams.map((team) => team.memberIds[0]))
    } else {
      return []
    }
  }, [usersInfoLoaded, selectedEventData])

  const isUserInEvent = useMemo(() => {
    if (teams && user && selectedEvent) {
      const found = teams.find((team) => team.eventId === selectedEvent.id)
      return found
    } else {
      return false
    }
  }, [teams, user, selectedEvent])

  // we use the syntax path={`${match.path}/something`} to match different base url for direct and past events
  return (
    <ContentPage>
      <Switch>
        <Route exact path={`${match.path}/participants`}>
          <EventParticipants
            event={selectedEvent}
            teams={selectedEventData.teams}
            users={eventUsersInfo}
            user={user}
          />
        </Route>
        <Route path={`${match.path}/classement/tour/:roundId`}>
          <EventRanking
            event={selectedEvent}
            rounds={selectedEventData.rounds}
            users={eventUsersInfo}
            order={selectedEvent ? selectedEvent.rankingType : undefined}
          />
        </Route>
        <Route exact path={`${match.path}/classement`}>
          <EventRanking
            event={selectedEvent}
            rounds={selectedEventData.rounds}
            users={eventUsersInfo}
            order={selectedEvent ? selectedEvent.rankingType : undefined}
          />
        </Route>

        {isLive && (
          <Route exact path={`${match.path}/stream`}>
            <div> base live</div>
          </Route>
        )}
        {isLive && (
          <Route exact path={`${match.path}/stream/:participantId`}>
            <div> live</div>
          </Route>
        )}
        <Route path={`${match.path}/`}>
          <EventInfo
            event={selectedEvent}
            onEventJoin={() => {
              joinEvent(selectedEvent.id, user.identity.username).then(
                (resp) => {
                  addNotification({
                    type: 'success',
                    message: `Vous êtes inscrit à ${selectedEvent.name}`,
                    duration: 10000
                  })
                  addTeamToSelectedEvent(resp.data.team)
                  addTeam(resp.data.team)
                }
              )
            }}
            onLeaveEvent={() => {
              leaveEvent(selectedEvent.id).then((resp) => {
                addNotification({
                  type: 'success',
                  message: `Vous êtes désinscrit de ${selectedEvent.name}`,
                  duration: 10000
                })

                deleteTeamToSelectedEvent(isUserInEvent.id)
                removeTeam(isUserInEvent.id)
              })
            }}
            isRegistered={isUserInEvent}
            user={user}
          />
        </Route>
      </Switch>
    </ContentPage>
  )
}

export default EventPage
