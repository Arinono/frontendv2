import React, { useEffect, useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import Media from '../ui/Media'
import UserInfo from './userInfo'
import Icon from '../ui/Icons/Icon'
import { convertApiRoundToDisplayRound } from '../utils/rounds'
import { useParams } from 'react-router-dom'

const RoundRanking = ({
  event,
  rounds,
  users,
  teams,
  rankings,
  order = 'SCORE_ASCENDING',
  roundsData = [],
  ...restProps
}) => {
  const { roundId } = useParams()

  useEffect(() => {
    console.log(roundId)
  }, [roundId])

  const teamsScores = useMemo(() => {
    if (roundsData && teams) {
      const data = convertApiRoundToDisplayRound(roundsData, roundId)

      return teams.map((t) => {
        return {
          ...t,
          ranking: data[t.id]
        }
      })
    }
    return null
  }, [roundsData, teams, roundId])

  const sortedTeams = useMemo(
    () =>
      teamsScores.sort((a, b) => {
        if (!a.ranking && !b.ranking) return 0
        if (!a.ranking) return order === 'SCORE_ASCENDING' ? -1 : 1
        if (!b.ranking) return order === 'SCORE_ASCENDING' ? 1 : -1
        return order === 'SCORE_ASCENDING'
          ? a.ranking.total - b.ranking.total
          : b.ranking.total - a.ranking.total
      }),
    [teamsScores]
  )

  const rankedTeams = useMemo(
    () =>
      sortedTeams.map((t, index) => ({
        ...t,
        rank: index + 1
      })),
    [sortedTeams]
  )

  return (
    <table {...restProps}>
      <Header>
        <tr>
          <RankHeader>
            <FormattedMessage id="event.ranking.position" />
          </RankHeader>
          <MediaHeader content={' '} />
          <PlayerHeader>
            <FormattedMessage id="event.ranking.player" />
          </PlayerHeader>
          <ScoreHeader>
            <FormattedMessage id="event.ranking.score" />
          </ScoreHeader>
        </tr>
      </Header>
      <tbody>
        {rankedTeams.map((team) => (
          <ContentRow key={team.id}>
            <RankField>{team.rank}</RankField>
            {team.members[0] ? (
              <MediaField>
                <StyledMedia
                  shape="circle"
                  src={team.members[0].identity.profileImageURL}
                  alt={team.members[0].identity.username}
                />
              </MediaField>
            ) : (
              <div></div>
            )}
            <UserField>
              {team.members[0] ? (
                <UserInfo
                  theme="dark"
                  userName={team.members[0].identity.username}
                  login={team.members[0].identity.login}
                  userLink={`https://www.twitch.tv/${team.members[0].identity.login}`}
                />
              ) : (
                <div></div>
              )}
            </UserField>

            <ScoreField>{team.ranking ? team.ranking.total : 0}</ScoreField>
          </ContentRow>
        ))}
      </tbody>
    </table>
  )
}

const Header = styled.thead`
  ${tw`text-blue italic border-b border-blue pb-2`}
`

const HeaderField = styled.th`
  ${tw`font-normal pb-2`}
`

const RankHeader = styled(HeaderField)`
  ${tw`w-12 pr-4 text-left`}
`

const PlayerHeader = styled(HeaderField)`
  ${tw`text-left`}
`

const ScoreHeader = styled(HeaderField)`
  ${tw`text-right`}
  width:15%;
`

const MediaHeader = styled(HeaderField)`
  ${tw`w-16 min-w-16  `}
`

const ContentField = styled.td`
  ${tw`font-normal text-white px-2 py-1`}
`

const RankField = styled(ContentField)`
  ${tw`w-12 text-center`}
`

const UserField = styled(ContentField)``

const ScoreField = styled(ContentField)`
  ${tw` text-right`}
  width: 15%;
`

const MediaField = styled(ContentField)`
  ${tw`w-16 min-w-16 mr-4 relative`}
`

const ContentRow = styled.tr`
  &:first-child {
    ${ContentField} {
      ${tw`pt-4`}
    }
  }

  &:not(:last-child) {
    ${ScoreField}:not(last-child),${UserField}:not(:last-child) {
      border-bottom: solid 1px rgba(255, 255, 255, 0.12);
    }
  }
  &:last-child {
    ${ContentField} {
      ${tw`pb-0`}
    }
  }
`

const StyledMedia = styled(Media)`
  ${tw`w-8 sm:w-12`}
  &.rank-1 {
    ${tw`border-4 border-blue rounded-full`}
  }
  &.rank-2 {
    ${tw`border-4 border-silver rounded-full`}
  }
  &.rank-3 {
    ${tw`border-4 border-bronze rounded-full`}
  }
`

export default RoundRanking
