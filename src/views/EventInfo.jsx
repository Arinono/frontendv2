import { format } from 'date-fns'
import { fr } from 'date-fns/locale'
import React, { useEffect, useMemo, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import Modal from '../components/Modal'
import config from '../config'
import Button from '../ui/Button'
import Card from '../ui/Card'
import ErrorMessage from '../ui/ErrorMessage'
import Paragraph from '../ui/Paragraph'
import Title from '../ui/Title'
import ReactMarkdown from 'react-markdown'
import EventTeamsManagementModal from '../components/modals/EventTeamsManagementModal'
import {
  getInvites,
  getMeTeamForEvent,
  getTeamInvites
} from '@breci/eventz_api'
import { useEvent } from '../store/event'

const DateFormat = 'PPPPp'

const EVENT_REGISTER_BUTTON_TYPE = {
  REGISTRATION_EXPIRED: 'REGISTRATION_EXPIRED'
}

const EventInfo = ({
  onEventJoin,
  onLeaveEvent,
  isRegistered = false,
  user
}) => {
  const { event, eventInitialized, eventMeTeamInitialized, loadMeTeam } =
    useEvent()
  const intl = useIntl()

  const [showUnregisterModal, setShowUnregisterModal] = useState(false)
  const [showTeamModal, setShowTeamModal] = useState(false)

  const [userTeam, setUserTeam] = useState(null)
  const [userInvites, setUserInvites] = useState([])

  // In case the event is not loaded
  useEffect(() => {
    if (!eventInitialized) {
      return
    } else if (!eventMeTeamInitialized) {
      loadMeTeam()
      return
    }
  }, [eventInitialized, eventMeTeamInitialized, loadMeTeam])

  const canRegister = (() => {
    if (!event) return false
    if (!event.isRegistrationOpen) return false
    if (user) {
      // event is open to everyone
      if (!event.neededIdentities) return true

      // something is wrong with the event, it needs another identity than just twitch
      if (Object.keys(event.neededIdentities).length > 1) return false

      const valid = event.neededIdentities.TWITCH.rules.every((rule) => {
        switch (rule.property) {
          case 'views':
            return rule.ruleType === 'BIGGER'
              ? rule.expected < user.identity.views
              : rule.expected > user.identity.views
          case 'followers':
            return rule.ruleType === 'BIGGER'
              ? rule.expected < user.identity.followers
              : rule.expected > user.identity.followers
          default:
            return false
        }
      })

      return valid
    } else {
      return false
    }
  })()

  const registerButton = (() => {
    if (!event) return null
    if (Date.now() < event.openRegistrationsAt * 1000)
      return (
        <InfoText>
          <FormattedMessage id="event.registration_not_started" />
        </InfoText>
      )
    if (Date.now() > event.closeRegistrationsAt * 1000)
      return (
        <InfoText>
          <FormattedMessage id="event.registration_ended" />
        </InfoText>
      )

    //Logged off user
    if (!user) {
      return (
        <InfoText>
          <FormattedMessage id="event.event_logged_off_message" />
        </InfoText>
      )
    }

    // TODO check for groupSize here

    if (event.groupSize > 0) {
      // TODO manage button text for when we have a team already or not
      return (
        <div>
          {showTeamModal && (
            <EventTeamsManagementModal
              user={user}
              event={event}
              team={userTeam}
              invites={userInvites}
              onClose={() => setShowTeamModal(false)}
            />
          )}
          <Button onClick={() => setShowTeamModal(true)}>
            <FormattedMessage id="event.event_open_team_management" />
          </Button>
        </div>
      )
    } else {
      if (isRegistered) {
        return (
          <div>
            {showUnregisterModal && (
              <Modal
                show={showUnregisterModal}
                message={intl.formatMessage({ id: 'event.unregister.message' })}
                cancelButtonText={intl.formatMessage({
                  id: 'event.unregister.cancel'
                })}
                confirmButtonText={intl.formatMessage({
                  id: 'event.unregister.ok'
                })}
                onClose={() => setShowUnregisterModal(false)}
                onConfirm={() => {
                  onLeaveEvent()
                  setShowUnregisterModal(false)
                }}
              />
            )}

            <Button hollow>
              <FormattedMessage id="event.event_joined_button" />
            </Button>
            <UnregisteredButton
              onClick={() => {
                setShowUnregisterModal(true)
              }}
            >
              <FormattedMessage id="event.event_unregister_button" />
            </UnregisteredButton>
          </div>
        )
      }
      if (!canRegister) {
        return (
          <NotEligibleContainer>
            <InfoText>
              <FormattedMessage id="event.not_eligible.message" />
            </InfoText>
            {event.neededIdentities && (
              <div>
                <PrerequisitesTable>
                  <thead>
                    <tr>
                      <th>
                        <FormattedMessage id="event.not_eligible.rule_header" />
                      </th>
                      <th>
                        <FormattedMessage id="event.not_eligible.prerequisite_header" />
                      </th>
                      <th>
                        <FormattedMessage id="event.not_eligible.user_header" />
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.entries(event.neededIdentities).map(
                      ([identity, data]) =>
                        data.rules.map(({ property, expected, ruleType }) => {
                          let rule = `event.registration_rules.${identity}_${property}_${ruleType}.`
                          let user_value = 'N/A'
                          if (user.identity[property] !== undefined) {
                            user_value = user.identity[property]
                          }
                          let operator = ''
                          if (ruleType === 'BIGGER') {
                            operator = '>'
                          }

                          return (
                            <tr key={'details_' + rule}>
                              <th>
                                <FormattedMessage id={rule + 'label'} />
                              </th>
                              <td>
                                {operator} {expected}
                              </td>
                              <td>{user_value}</td>
                            </tr>
                          )
                        })
                    )}
                  </tbody>
                </PrerequisitesTable>
                <ReconnectNotice>
                  <FormattedMessage id="event.not_eligible.notice" />
                  <StyledLink
                    href={`https://id.twitch.tv/oauth2/authorize?client_id=${config.API_CLIENT_ID_FIX}&redirect_uri=${window.location.origin}/auth/twitch&response_type=token&scope=user:read:email`}
                  >
                    <FormattedMessage id="event.not_eligible.reconnect_link" />
                  </StyledLink>
                </ReconnectNotice>
              </div>
            )}
          </NotEligibleContainer>
        )
      }
    }

    return (
      <Button onClick={onEventJoin}>
        <FormattedMessage id="event.join_button" />
      </Button>
    )
  })()

  // TODO optimize with hooks
  //region title
  const year = useMemo(() => {
    if (!event) return ''
    return format(event.startEventAtDate, 'yyyy', { locale: fr })
  }, [event])
  let date_title = useMemo(() => {
    if (!event) return null
    return (
      <div>
        {format(event.startEventAtDate, 'PPPP', { locale: fr }).replace(
          year,
          ''
        )}
        <Year>{year}</Year>
      </div>
    )
  }, [event, year])
  //endregion

  //region event dates
  let event_start_label = event
    ? format(event.startEventAtDate, DateFormat, {
        locale: fr
      })
    : ''
  let event_end_label = event
    ? format(event.endEventAtDate, DateFormat, {
        locale: fr
      })
    : ''
  let event_dates = useMemo(() => {
    if (!event) return null
    return event.lastOneDay ? (
      <Paragraph label={'event.date'}>{event_start_label}</Paragraph>
    ) : (
      <InformationRow>
        <Paragraph label={'event.event_start_date'}>
          {event_start_label}
        </Paragraph>
        <Paragraph label={'event.event_end_date'}>{event_end_label}</Paragraph>
      </InformationRow>
    )
  }, [event, event_start_label, event_end_label])
  //endregion

  //region registration dates

  const registration_dates = useMemo(() => {
    if (!event) return ''
    if (event.isFuture) {
      let registration_start_label = format(
        event.openRegistrationsAtDate,
        DateFormat,
        { locale: fr }
      )
      let registration_end_label = format(
        event.closeRegistrationsAtDate,
        DateFormat,
        { locale: fr }
      )

      return (
        <InformationRow>
          <Paragraph label={'event.registration_start_date'}>
            {registration_start_label}
          </Paragraph>
          <Paragraph label={'event.registration_end_date'}>
            {registration_end_label}
          </Paragraph>
        </InformationRow>
      )
    }
  }, [event])

  //endregion

  if (!event) {
    return <ErrorMessage title={'events.no_events'} message={'Ya rien'} />
  }

  return (
    <div className="w-full">
      {event && (
        <div>
          <Title center="true">
            {event.name} <br /> {date_title}
          </Title>

          <CardRow>
            <Card>
              <Title level={2}>
                <FormattedMessage id="event.summary_title" />
              </Title>
              <StyledArticle className="prose">
                <ReactMarkdown source={event.description} />
              </StyledArticle>
              {registerButton}
            </Card>
            <Card>
              <div>
                <Title level={2}>
                  <FormattedMessage id="event.information_title" />
                </Title>
                {registration_dates}
                {event_dates}
                {event.neededIdentities && (
                  <div>
                    <Title level={3}>
                      <FormattedMessage id="event.registration_rules_title" />
                    </Title>
                    <InformationRow>
                      {Object.entries(event.neededIdentities).map(
                        ([identity, data]) => {
                          return data.rules.map(
                            ({ property, expected, ruleType }) => {
                              let rule = `event.registration_rules.${identity}_${property}_${ruleType}.`
                              return (
                                <Paragraph label={rule + 'label'} key={rule}>
                                  <FormattedMessage
                                    id={rule + 'value'}
                                    values={{ value: expected }}
                                  />
                                </Paragraph>
                              )
                            }
                          )
                        }
                      )}
                    </InformationRow>
                  </div>
                )}
              </div>
            </Card>
          </CardRow>
          {event.isLive && (
            <CardRow>
              <Card style={{ width: '100%' }}>
                <Title level={2}>
                  <FormattedMessage id="event.live_title" />
                </Title>
                <StyledIframe
                  src={
                    'https://player.twitch.tv/?channel=zerator&parent=' +
                    [encodeURIComponent(window.location.hostname)].join(',')
                  }
                  frameBorder="0"
                  scrolling="no"
                  allowFullScreen={true}
                  height="400rem"
                />
              </Card>
            </CardRow>
          )}
        </div>
      )}
    </div>
  )
}

const NotEligibleContainer = styled.div`
  ${tw`p-2 border border-gray rounded m-auto`}
`
const PrerequisitesTable = styled.table`
  ${tw`text-xs opacity-50 w-full mt-3 text-left`}
`
const ReconnectNotice = styled.div`
  ${tw`text-xs opacity-50 w-full my-3 text-center`}
`
const StyledLink = styled.a`
  ${tw`block underline`}
`

const CardRow = styled.div`
  ${tw`flex mb-4 flex-wrap`}
  &> ${Card} {
    ${tw`flex-1 m-2`}
    min-width: 15rem;
  }
`

const StyledIframe = styled.iframe`
  ${tw`w-full mt-6 mb-3`}
`

const StyledArticle = styled.article`
  ${tw`my-8 max-w-full text-base leading-snug`}
  strong {
    ${tw`text-primary`}
  }
`

const InformationRow = styled.div`
  ${tw`flex flex-row flex-wrap items-start content-start my-4`}
  & > div {
    ${tw`mr-4`}
  }
`

const UnregisteredButton = styled(Button)`
  ${tw`bg-red border-red mt-4`}
`

const Year = styled.span`
  ${tw`text-primary`}
`

const InfoText = styled.div`
  ${tw`text-xs opacity-50 w-full text-center`}
`
export default EventInfo
