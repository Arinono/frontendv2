import React from 'react'
import { format } from 'date-fns'
import { fr } from 'date-fns/locale'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import tw from 'twin.macro'
import Title from '../ui/Title'
import Button from '../ui/Button'
import { FormattedMessage } from 'react-intl'

const HomeFutureEvents = ({ events, size = 'medium' }) => (
  <Container>
    {events
      .sort((a, b) => {
        return a.startEventAt > b.startEventAt
      })
      .map((event) => {
        let date_label_key = event.lastOneDay
          ? 'dates.same_day_label'
          : 'dates.range_label'
        let date_label_values = {}

        if (event.lastOneDay) {
          date_label_values = {
            date: format(
              event.startEventAtDate,
              size === 'medium' ? 'PPPP' : 'P',
              { locale: fr }
            ),
            start_hour: format(
              event.startEventAtDate,
              size === 'medium' ? 'p' : 'p',
              { locale: fr }
            ),
            end_hour: format(
              event.startEventAtDate,
              size === 'medium' ? 'p' : 'p',
              { locale: fr }
            )
          }
        } else {
          date_label_values = {
            start: format(
              event.startEventAtDate,
              size === 'medium' ? 'PPPPp' : 'Pp',
              { locale: fr }
            ),
            end: format(
              event.endEventAtDate,
              size === 'medium' ? 'PPPPp' : 'Pp',
              { locale: fr }
            )
          }
        }

        return (
          <Event key={event.id} to={'/evenement/' + event.id}>
            <StyledTitle level={3} style={tw`z-10`}>
              {event.name}
            </StyledTitle>
            <DateDisplay>
              <FormattedMessage
                id={date_label_key}
                values={date_label_values}
              />
            </DateDisplay>
            <Button size="sm">
              <FormattedMessage id="home.learn_more" />
            </Button>
          </Event>
        )
      })}
  </Container>
)

const Container = styled.div`
  ${tw`mt-2 mb-3 mx-auto w-1/2 flex flex-wrap items-stretch content-center justify-around`}
`

const Event = styled(Link)`
  ${tw`bg-black bg-opacity-50 p-4 mb-3 rounded shadow text-white block self-stretch flex flex-col flex-nowrap items-stretch`}
  width: 48%;
  min-width: 15rem;
`

const StyledTitle = styled(Title)`
  ${tw`flex-none truncate`}
`
const DateDisplay = styled.div`
  ${tw`opacity-50 text-sm m-3 flex-grow text-center`}
`

export default HomeFutureEvents
