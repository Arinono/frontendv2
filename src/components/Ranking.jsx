import React, { useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import Media from '../ui/Media'
import Title from '../ui/Title'
import UserInfo from './userInfo'
import Icon from '../ui/Icons/Icon'
import { convertApiRoundToDisplayRound } from '../utils/rounds'
import { useMediaQuery } from '@react-hook/media-query'

const Ranking = ({
  event,
  rounds,
  users,
  teams,
  rankings,
  order = 'SCORE_ASCENDING',
  roundsData = [],
  ...restProps
}) => {
  const isMobile = !useMediaQuery('(min-width: 640px)')

  // TODO handle solo team AND multiple members teams
  const isSolo = event.groupSize === 1

  const nbRounds = useMemo(() => roundsData.length, [rounds])

  const teamsScores = useMemo(() => {
    if (roundsData && teams) {
      const data = convertApiRoundToDisplayRound(roundsData)

      return teams.map((t) => {
        return {
          ...t,
          ranking: data[t.id]
        }
      })
    }
    return null
  }, [roundsData, teams])

  const sortedTeams = useMemo(
    () =>
      teamsScores.sort((a, b) => {
        if (!a.ranking && !b.ranking) return 0
        if (!a.ranking) return order === 'SCORE_ASCENDING' ? -1 : 1
        if (!b.ranking) return order === 'SCORE_ASCENDING' ? 1 : -1
        return order === 'SCORE_ASCENDING'
          ? a.ranking.total - b.ranking.total
          : b.ranking.total - a.ranking.total
      }),
    [teamsScores]
  )

  const rankedTeams = useMemo(
    () =>
      sortedTeams.map((t, index) => ({
        ...t,
        rank: index + 1
      })),
    [sortedTeams]
  )

  return (
    <table {...restProps}>
      <Header>
        <tr>
          <RankHeader>
            <FormattedMessage id="event.ranking.position" />
          </RankHeader>
          {isSolo && <MediaHeader content={' '} />}
          <PlayerHeader>
            <FormattedMessage id="event.ranking.player" />
          </PlayerHeader>
          {!isMobile &&
            new Array(nbRounds)
              .fill(0)
              .map((item, index) => (
                <ScoreHeader key={`round-${index + 1}`}>
                  Round {(index + 1).toString()}
                </ScoreHeader>
              ))}
          <ScoreHeader>
            <FormattedMessage id="event.ranking.global" />
          </ScoreHeader>
        </tr>
      </Header>
      <tbody>
        {rankedTeams.map((team) => (
          <ContentRow key={team.id}>
            <RankField>{team.rank}</RankField>
            {isSolo ? (
              <MediaField>
                <StyledMedia
                  shape="circle"
                  src={team.members[0].identity.profileImageURL}
                  alt={team.members[0].identity.username}
                  className={event.isPast && `rank-${team.rank}`}
                />
                {event.isPast && team.rank === 1 && <StyledIcon icon="crown" />}
              </MediaField>
            ) : null}
            <UserField>
              {isSolo ? (
                <UserInfo
                  theme="dark"
                  userName={team.members[0].identity.username}
                  login={team.members[0].identity.login}
                  userLink={`https://www.twitch.tv/${team.members[0].identity.login}`}
                />
              ) : (
                <div className="relative">
                  {team.rank <= 3 && (
                    <TeamStyledIcon
                      icon="crown"
                      className={event.isPast && `rank-${team.rank}`}
                    />
                  )}
                  <div>{team.name}</div>
                </div>
              )}
            </UserField>
            {!isMobile &&
              new Array(nbRounds)
                .fill(0)
                .map((item, index) => (
                  <ScoreField key={`round-${index + 1}`}>
                    {team.ranking && team.ranking.scores[index] !== undefined
                      ? team.ranking.scores[index]
                      : ''}
                  </ScoreField>
                ))}
            <ScoreField>{team.ranking ? team.ranking.total : 0}</ScoreField>
          </ContentRow>
        ))}
      </tbody>
    </table>
  )
}

const Header = styled.thead`
  ${tw`text-blue italic border-b border-blue pb-2`}
`

const HeaderField = styled.th`
  ${tw`font-normal pb-2`}
`

const RankHeader = styled(HeaderField)`
  ${tw`w-12 pr-4 text-left`}
`

const PlayerHeader = styled(HeaderField)`
  ${tw`text-left`}
`

const ScoreHeader = styled(HeaderField)`
  ${tw`text-right`}
  width:15%;
`

const MediaHeader = styled(HeaderField)`
  ${tw`w-8 sm:w-12`}
`

const ContentField = styled.td`
  ${tw`font-normal text-white px-2 py-1`}
`

const RankField = styled(ContentField)`
  ${tw`w-12 text-center`}
`

const UserField = styled(ContentField)``

const ScoreField = styled(ContentField)`
  ${tw` text-right`}
  width: 15%;
`

const MediaField = styled(ContentField)`
  ${tw`w-8 sm:w-12 mr-4 relative`}
`

const ContentRow = styled.tr`
  &:first-child {
    ${ContentField} {
      ${tw`pt-4`}
    }
  }

  &:not(:last-child) {
    ${ScoreField}:not(last-child),${UserField}:not(:last-child) {
      border-bottom: solid 1px rgba(255, 255, 255, 0.12);
    }
  }
  &:last-child {
    ${ContentField} {
      ${tw`pb-0`}
    }
  }
`

const StyledMedia = styled(Media)`
  ${tw`w-8 sm:w-12`}
  &.rank-1 {
    ${tw`border-4 border-blue rounded-full`}
  }
  &.rank-2 {
    ${tw`border-4 border-silver rounded-full`}
  }
  &.rank-3 {
    ${tw`border-4 border-bronze rounded-full`}
  }
`

const StyledIcon = styled(Icon)`
  ${tw`h-4 fill-current text-blue absolute top-0 right-1/2`}
  transform: rotate(9.48deg) translateX(70%) translateY(0.5rem);
`
const TeamStyledIcon = styled(Icon)`
  ${tw`h-4 fill-current text-blue absolute top-0 left-0`}
  transform: rotate(-9.48deg) translateY(-1rem) translateX(-1rem);
  &.rank-1 {
    ${tw`text-blue`}
  }
  &.rank-2 {
    ${tw`text-silver`}
  }
  &.rank-3 {
    ${tw`text-bronze`}
  }
`

export default Ranking
