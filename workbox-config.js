module.exports = {
  globDirectory: 'build/',
  globPatterns: ['**/*.{js,svg,png,jpg,css,ico,eot,ttf,woff,woff2,html,txt}'],
  swDest: 'build/sw.js'
}
