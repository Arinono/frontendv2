import React, { useEffect, useMemo, useRef, useState } from 'react'
import { useUser } from '../store/user'
import { setAuthToken } from '@breci/eventz_api'

const AuthManager = ({ children }) => {
  // check if token on load

  const { token, loadUserInfo } = useUser()
  const [initialized, setInitialized] = useState(false)

  const isTwitchAuthPage = useMemo(
    () => window.location.pathname === '/auth/twitch',
    []
  )

  useEffect(() => {
    if (token) {
      setAuthToken(token)
      setInitialized(true)
    }
  }, [token])
  useEffect(() => {
    if (token && initialized && !isTwitchAuthPage) {
      loadUserInfo().then(() => {
        if (localStorage.getItem('AUTH_PREVIOUS_URL')) {
          window.location.replace(localStorage.getItem('AUTH_PREVIOUS_URL'))
          localStorage.removeItem('AUTH_PREVIOUS_URL')
        }
      })
    }
  }, [token, initialized, loadUserInfo, isTwitchAuthPage])

  return <>{children}</>
}

export default AuthManager
