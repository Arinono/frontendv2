import React from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import CryingIcon from './Icons/CryingIcon'
import Title from './Title'

const ErrorMessage = (props) => {
  return (
    <Container>
      <StyledCryingIcon />
      <StyledTitle level={2}>
        <FormattedMessage id={props.title} />
      </StyledTitle>
      {props.message}
    </Container>
  )
}

const StyledCryingIcon = styled(CryingIcon)`
  ${tw`w-40 mx-auto mb-6`}
`

const StyledTitle = styled(Title)`
  ${tw`mb-6 uppercase text-center text-2xl`}
  &:after {
    ${tw`w-32 right-auto -ml-16 left-1/2`}
  }
`

const Container = styled.div`
  ${tw`text-center p-4`}
`

export default ErrorMessage
