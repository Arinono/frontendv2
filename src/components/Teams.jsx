import React, { useEffect, useMemo } from 'react'
import { Link } from 'react-router-dom'
import tw from 'twin.macro'
import { useEvents } from '../store/events'
import Icon from '../ui/Icons/Icon'
import styled from 'styled-components'
import nbs from 'human-readable-numbers'

const ANONYMOUS_IDENTITY = {
  // default identity to have a placeholder
  profileImageURL: '',
  username: 'Anonyme',
  login: 'zerator',
  views: 0,
  followers: 0
}

const MultipleUsersTeam = ({ team }) => {
  const description = useMemo(() => {
    const unknownUsers =
      team.memberIds.length - team.identities.filter(Boolean).length
    if (unknownUsers === team.memberIds.length) return `${unknownUsers} membres`
    const names = team.identities
      .filter(Boolean)
      .map((i) => i.identity.username)

    return `${names.join(', ')} + ${unknownUsers}`
  }, [team.identities, team.memberIds])
  return (
    <Team>
      <Information>
        <Name>{team.name}</Name>
        <Description>{description}</Description>
      </Information>
    </Team>
  )
}

const SingleMemberTeam = ({ team }) => {
  const isAnonymous = useMemo(() => {
    return !team.identities[0]
  }, team.identities)
  const user = !isAnonymous
    ? team.identities[0]
    : {
        id: team.id,
        identity: ANONYMOUS_IDENTITY
      }

  return (
    <Team>
      <Avatar background={user.identity.profileImageURL} />
      <Information>
        <Name>{user.identity.username}</Name>
        <SocialLink
          target="_blank"
          href={`https://www.twitch.tv/${user.identity.login}`}
        >
          twitch.tv/
          {user.identity.login}
        </SocialLink>
        <Counts>
          <Count>
            <Icon icon="eye" />
            {!isAnonymous ? nbs.toHumanString(user.identity.views) : '?'}
          </Count>
          <Count>
            <Icon icon="people" />
            {!isAnonymous ? nbs.toHumanString(user.identity.followers) : '?'}
          </Count>
        </Counts>
      </Information>
    </Team>
  )
}

const Teams = ({ teams = [], isSolo = true }) => {
  // sort teams by viewCount
  const sortedTeams = useMemo(() => {
    const totalViewsTeams = teams.map((t) => ({
      ...t,
      total: t.identities.reduce((total, i) => {
        // compute total views from the members
        return i ? total + i.identity.views || 0 : total
      }, 0)
    }))
    return totalViewsTeams.sort((a, b) => b.total - a.total)
  }, [teams])

  return (
    <Container>
      {isSolo
        ? sortedTeams.map((team) => (
            <SingleMemberTeam key={team.id} team={team} />
          ))
        : sortedTeams.map((team) => (
            <MultipleUsersTeam key={team.id} team={team} />
          ))}
    </Container>
  )
}

const Container = styled.div`
  ${tw`flex flex-row flex-wrap my-6`}
`

const Team = styled.div`
  ${tw`p-2 inline-flex flex-row flex-nowrap items-center`}
  width:14rem;
`

const Avatar = styled.div`
  ${tw`w-12 h-12 flex-none rounded-full bg-center bg-cover bg-repeat bg-blue-dark mr-2`}
  ${(props) =>
    props.background && 'background-image: url(' + props.background + ')'}
`

const Information = styled.div`
  ${tw`flex-grow flex flex-col`}
`

const Name = styled.div`
  ${tw`leading-tight font-bold`}
`

const SocialLink = styled.a`
  ${tw`text-primary leading-none text-sm`}
`

const Counts = styled.div`
  ${tw`text-gray text-sm`}
  svg {
    ${tw`w-4 fill-gray inline-block mr-1`}
  }
`

const Count = styled.div`
  ${tw`mr-2 inline-block`}
`

const Description = tw.div`opacity-50 text-sm`

export default Teams
