import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

const Dropdown = ({ options = [], children }) => {
  const [showMenu, setShowMenu] = useState(false)
  const node = useRef()

  // manage click outside of the dropdown
  const handleClick = (e) => {
    if (node.current.contains(e.target)) {
      // inside click
      return
    }
    // outside click
    setShowMenu(false)
  }

  const onContainerClick = (e) => {
    setShowMenu((v) => !v)
  }
  useEffect(() => {
    // add when mounted
    document.addEventListener('mousedown', handleClick)
    // return function to be called when unmounted
    return () => {
      document.removeEventListener('mousedown', handleClick)
    }
  }, [])
  return (
    <Container ref={node}>
      <MainContainer onClick={onContainerClick}>
        <div>{children}</div>
        <div>▼</div>
      </MainContainer>
      {showMenu && (
        <DropDownChild>
          {options.map((option, idx) => (
            <DropDownOption key={idx} onClick={option.onClick}>
              {option.render || option.name}
            </DropDownOption>
          ))}
        </DropDownChild>
      )}
    </Container>
  )
}

const Container = styled.div`
  position: relative;
  display: inline-block;
`

const DropDownChild = styled.div`
  ${tw`inline-flex flex-col shadow`}
  position: absolute;
  top: 100%;
  right: 0;
  width: 100%;
  margin-top: 0.2rem;
`

const DropDownOption = styled.button`
  overflow: hidden;
  ${tw`p-2`}
  &:focus {
    ${tw` outline-none`}
  }

  &:first-child {
    ${tw` rounded-t`}
  }
  &:last-child {
    ${tw` rounded-b`}
  }

  &:nth-child(even) {
    ${tw`bg-primary`}
  }

  &:nth-child(odd) {
    ${tw`bg-primary-dark`}
  }
`

const MainContainer = styled.button`
  ${tw`inline-flex flex-row items-center outline-none`}
  ${DropDownChild} {
    ${tw`hidden`}
  }
  &:focus {
    ${tw` outline-none`}
    ${DropDownChild} {
      ${tw`flex `}
    }
  }
`

export default Dropdown
