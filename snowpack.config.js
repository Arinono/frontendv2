module.exports = {
  extends: '@snowpack/app-scripts-react',
  scripts: {
    'build:js,jsx': '@snowpack/plugin-babel',
    'build:css': 'postcss',
    'bundle:*': '@snowpack/plugin-webpack'
  },
  plugins: [
    [
      '@snowpack/plugin-webpack',
      {
        extendConfig: (config) => {
          config.output.publicPath = '/'
          return config
        }
      }
    ],
    '@snowpack/plugin-dotenv'
  ],
  install: [],
  installOptions: {
    rollup: {
      plugins: [
        require('rollup-plugin-node-polyfills')(),
        require('./plugins/cleanWarns')()
      ]
    }
  }
}
