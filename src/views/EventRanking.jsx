import React, { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import tw from 'twin.macro'
import Ranking from '../components/Ranking'
import Card from '../ui/Card'
import ErrorMessage from '../ui/ErrorMessage'
import Title from '../ui/Title'
import RoundRanking from '../components/RoundRanking'
import { useParams, Link, useRouteMatch } from 'react-router-dom'
import { AnimateSharedLayout, motion } from 'framer-motion'
import { useMediaQuery } from '@react-hook/media-query'
import { useEvent } from '../store/event'

function formatUrl(path, params, roundId) {
  return Object.entries({ ...params, roundId: roundId }).reduce(
    (reducer, [key, value]) => reducer.replace(`:${key}`, value),
    path
  )
}

function goBackToGlobalRanking(url) {
  const separator = '/classement'
  const pos = url.indexOf(separator)
  return url.substring(0, pos + separator.length)
}

const spring = {
  type: 'spring',
  stiffness: 500,
  damping: 30
}
const AnimatedUnderLine = () => {
  return (
    <UnderLineContainer transition={spring} layoutId="nav-rounds-underline">
      <UnderLine />
    </UnderLineContainer>
  )
}

const EventRanking = ({ users, order = 'SCORE_ASCENDING' }) => {
  const isMobile = !useMediaQuery('(min-width: 640px)')
  const { roundId } = useParams()
  const match = useRouteMatch()

  const [roundsRanks] = useState([])

  const {
    event,
    rounds,
    rankings,

    extendedTeamsWithUsersInfo,
    extendedRankingsWithUsersInfo,
    extendedRoundWithRankingsAndUserInfo,

    eventInitialized,
    eventMeTeamInitialized,
    loadMeTeam,
    loadRounds,
    loadTeams,
    loadUserDataFromRounds,
    loadRankings,

    eventTeamsInitialized,
    usersInfoInitialized,
    eventRoundsInitialized,
    rankingInitialized
  } = useEvent()

  useEffect(() => {
    if (!eventInitialized) {
      return
    } else if (!eventMeTeamInitialized) {
      loadMeTeam()
      return
    } else if (!eventRoundsInitialized) {
      loadRounds()
      return
    } else if (!eventTeamsInitialized) {
      loadTeams()
      return
    } else if (!usersInfoInitialized) {
      loadUserDataFromRounds()
      return
    } else if (!rankingInitialized) {
      loadRankings()
    }
  }, [
    eventInitialized,
    eventMeTeamInitialized,
    eventRoundsInitialized,
    eventTeamsInitialized,
    usersInfoInitialized,
    rankingInitialized,
    loadMeTeam,
    loadRounds,
    loadTeams,
    loadUserDataFromRounds,
    loadRankings
  ])

  useEffect(() => {}, [
    rounds,
    event,
    extendedRankingsWithUsersInfo,
    extendedTeamsWithUsersInfo,
    extendedRoundWithRankingsAndUserInfo
  ])
  //

  // In case the event is not loaded
  if (!event || !extendedRoundWithRankingsAndUserInfo) {
    return <ErrorMessage title={'events.no_events'} message={'Ya rien'} />
  }

  return (
    <div className="w-full">
      <Title center={isMobile}>{event.name}</Title>
      <Card style={{ width: '100%' }}>
        <Title level={2}>
          <FormattedMessage id="event.ranking.title" />
        </Title>
        <AnimateSharedLayout>
          <RankNavigation>
            {rounds.map((round) => (
              <div style={{ position: 'relative' }}>
                <RoundNavLink
                  key={round.id}
                  to={
                    !roundId
                      ? `${match.url}/tour/${round.id}`
                      : formatUrl(match.path, match.params, round.id)
                  }
                  selected={roundId === round.id}
                >
                  {round.name}
                </RoundNavLink>
                {roundId === round.id && <AnimatedUnderLine />}
              </div>
            ))}
            <div style={{ position: 'relative' }}>
              <RoundNavLink to={goBackToGlobalRanking(match.url)}>
                <FormattedMessage id="event.ranking.global" />
              </RoundNavLink>
              {!roundId && <AnimatedUnderLine />}
            </div>
          </RankNavigation>
        </AnimateSharedLayout>

        {roundId ? (
          <StyledRoundRanking
            event={event}
            rounds={roundsRanks}
            users={users}
            order={order}
            roundId={roundId}
            teams={extendedTeamsWithUsersInfo}
            roundsData={extendedRoundWithRankingsAndUserInfo}
          />
        ) : (
          <StyledRanking
            event={event}
            rounds={roundsRanks}
            roundsData={extendedRoundWithRankingsAndUserInfo}
            users={users}
            teams={extendedTeamsWithUsersInfo}
            rankings={rankings}
            order={order}
          />
        )}
      </Card>
    </div>
  )
}

const UnderLine = styled.div`
  width: 80%;
  height: 0.2rem;
  ${tw`bg-primary rounded`}
`

const UnderLineContainer = styled(motion.div)`
  ${tw`absolute w-full flex justify-center`}
`

const RoundNavLink = styled(Link)`
  ${tw`mx-1 uppercase relative`}
`

const RankNavigation = styled.div`
  ${tw`flex justify-end mb-4 relative`}
`

const StyledRoundRanking = styled(RoundRanking)`
  ${tw`w-full`}
`

const StyledRanking = styled(Ranking)`
  ${tw`w-full`}
`

export default EventRanking
