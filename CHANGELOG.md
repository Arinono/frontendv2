# [1.2.0](https://gitlab.com/fightforsub/frontendv2/compare/v1.1.1...v1.2.0) (2021-11-29)


### Features

* **auth:** add logout behaviour ([0886e4f](https://gitlab.com/fightforsub/frontendv2/commit/0886e4f91f6c6cf11f73c7d873c4c1af64c5f507))

## [1.1.1](https://gitlab.com/fightforsub/frontendv2/compare/v1.1.0...v1.1.1) (2021-11-29)


### Bug Fixes

* **auth-dropdown:** remove 'a' tag when logged in ([3b84689](https://gitlab.com/fightforsub/frontendv2/commit/3b8468907c448c1f634d827eb8d023f3ee45a9ff))
* console errors on list key and link props ([bc08ea0](https://gitlab.com/fightforsub/frontendv2/commit/bc08ea01cf3875e075236b6d5db6e1d458af8d6f))

# [1.1.0](https://gitlab.com/fightforsub/frontendv2/compare/v1.0.0...v1.1.0) (2021-11-29)


### Features

* using webpack instead of snowpack ([f6977a3](https://gitlab.com/fightforsub/frontendv2/commit/f6977a31a15e243d7ae27ab0f08f6cad7a09e176))

# 1.0.0 (2021-11-18)


### Features

* using webpack instead of snowpack ([5a35ba0](https://gitlab.com/fightforsub/frontendv2/commit/5a35ba06c056187527c0dfcc2b5931245031a79a))
