import styled from 'styled-components'
import tw from 'twin.macro'

const ContentPage = styled.div`
  ${tw` pt-16 sm:pt-32 px-4 sm:px-8 md:px-16 lg:px-32 xl:px-32 flex flex-col items-center container mx-auto`}
`

export default ContentPage
