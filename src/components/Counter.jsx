import React from 'react'
import Button from '../ui/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'
import tw from 'twin.macro'

const Counter = ({ value, onIncrease, onDecrease }) => (
  <Container>
    <Value>{value}</Value>
    <div>
      <Button onClick={onDecrease}>
        <FontAwesomeIcon icon="minus" />
      </Button>
      <Button onClick={onIncrease}>
        <FontAwesomeIcon icon="plus" />
      </Button>
    </div>
  </Container>
)

const Container = styled.div`
  ${tw`flex items-center justify-center text-center flex-col`}
`

const Value = styled.span`
  ${tw`font-bold text-xl`}
`

export default Counter
