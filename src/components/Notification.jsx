import React from 'react'
import styled from 'styled-components'
import tw from 'twin.macro'

const Notification = ({ notification, onClose }) => {
  return (
    <Container type={notification.type}>
      <Message>{notification.message}</Message>
      {onClose && (
        <div>
          <InvisibleButton onClick={onClose}>&times;</InvisibleButton>
        </div>
      )}
    </Container>
  )
}

const InvisibleButton = styled.button`
  &:focus {
    outline: none !important;
  }
`

const Message = styled.div`
  flex: 1;
`

const Container = styled.div`
  ${tw`p-4 flex`}
  ${({ type = 'success' }) => {
    switch (type) {
      case 'success':
        return tw`bg-green text-white`
      case 'error':
        return tw`bg-red text-white`
      case 'warning':
        return tw`bg-gray text-white`
    }
  }}
`

export default Notification
